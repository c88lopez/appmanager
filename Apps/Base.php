<?php
/**
 * Base.php
 * Contiene la clase abstracta Base
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Apps
 * @subpackage Base
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Apps;

/**
 * Base
 * Esta clase contiene la logica basica para la clase Main de todas las aplicaciones
 *
 * @category   AppManager
 * @package    Apps
 * @subpackage Base
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
abstract class Base
{
    /**
     * Properties
     */
    /**
     * Representa la instancia a la clase Request
     * @var object
     */
    public $oRequest;
    /**
     * Representa la instancia a la clase Session
     * @var object
     */
    public $oSession;
    /**
     * Representa la instancia a la clase DB
     * @var object
     */
    public $oDB;

    /**
     * Methods
     */
    
    /**
     * Metodo constructor
     */
    public function __construct()
    {
        $this->initApp();
    }

    private function initApp()
    {
        $this->oRequest = new \Core\Action\Request();
        $this->oSession = new \Core\Storage\Session();
        $this->oDB = new \Core\Storage\DB();
    }
    
    /**
     * Metodo que se encarga de retornar el nombre de la aplicacion
     * 
     * @param string $sAppNamespace El namespace que le corresponde a la aplicacion
     * 
     * @return string
     */
    public function getAppNameByNamespace($sAppNamespace)
    {
        $aAppNamespaceParts = explode('\\', $sAppNamespace);

        return end($aAppNamespaceParts);
    }
    
    /**
     * Metodo que getter de la variable $_sAppName
     * 
     * @return string
     */
    public function getAppName()
    {
        return $this->sAppName;
    }
}