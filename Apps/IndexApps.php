<?php

/**
 * IndexApps.php
 * Archivo que contiene la clase IndexApps
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Apps
 * @subpackage IndexApps
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Apps;

/**
 * IndexApps
 * Clase que se encarga de mostrar las aplicaciones disponibles.
 *
 * @category Index
 * @package  AppManager
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  nolicense No license
 * @link     http://nolink.com
 */
class IndexApps
{
    /**
     * Properties
     */
    /**
     * Contiene la lista de applicaciones
     * @var array
     */
    protected static $aAppsList = array();

    /**
     * Methods
     */
    
    /**
     * Metodo setter de la variable $aAppsList
     *
     * @param array $aValue El listado de aplicaciones existentes
     */
    public static function setAppsList($aValue)
    {
        $iCantApps = count($aValue);
        for($i=0 ; $i < $iCantApps ; $i++) {
            if (\Core\Validation\Apps::isExecutable($aValue[$i]['path']) 
                && \Core\Validation\Apps::isActive($aValue[$i]['path'])) {
                self::$aAppsList[] = $aValue[$i];
            }
        }
    }

    /**
     * Metodo getter de la variable $aAppsList
     *
     * @return array
     */
    public static function getAppsList()
    {
        return self::$aAppsList;
    }

    /**
     * Metodo que se encarga de listar las aplicaciones disponibles
     * 
     * @return void
     */
    public static function showApps()
    {
        self::setAppsList(\Core\Router\Apps::getAvailableApps());
        \Core\Template\Engine::render(__METHOD__);
    }
    

    /**
     * Metodo que se encarga de listar las aplicaciones disponibles
     * 
     * @return void
     */
    public function run()
    {
        self::showApps();
    }
}