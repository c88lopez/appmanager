1
00:00:36,796 --> 00:00:39,185
- John.
- Ally. �Qu� est�s haciendo aqu�?

2
00:00:39,436 --> 00:00:44,749
- Trabajando. �Estabas aqu�?
- S�. Ando algo atareado �ltimamente.

3
00:00:44,956 --> 00:00:46,867
�Por los restos?

4
00:00:47,756 --> 00:00:53,786
- Retrasado en el trabajo.
- Ah, est� bien. Yo tambi�n.

5
00:00:57,716 --> 00:01:00,389
- �Perdona?
- No he dicho nada.

6
00:01:02,556 --> 00:01:06,788
- �Qu� pasa, Ally?
- Nada. Aqu�, trabajando hasta tarde.

7
00:01:07,036 --> 00:01:10,153
Ya ni me gusta salir con chicos.
No pasa nada.

8
00:01:10,396 --> 00:01:13,547
Afuera cae una ligera
y preciosa nieve. . .

9
00:01:13,796 --> 00:01:17,311
. . .y aqu� estamos nosotros,
siendo abogados hasta el final.

10
00:01:17,516 --> 00:01:20,747
Me promet� que nunca
llegar�a a ser como mi padre.

11
00:01:24,516 --> 00:01:26,552
- �C�mo es eso?
- �El qu�?

12
00:01:26,796 --> 00:01:31,267
�C�mo podemos estar t� y yo
cada d�a, cara a cara,. .

13
00:01:31,676 --> 00:01:35,954
. . .y no vemos lo que est� justo
delante de nuestras narices?

14
00:02:17,356 --> 00:02:19,153
Hola.

15
00:02:19,356 --> 00:02:22,348
- �Est�s despierta?
- �Qu� haces t� en mi cama?

16
00:02:23,756 --> 00:02:26,987
- He tenido un sue�o.
- Contame.

17
00:02:27,756 --> 00:02:32,432
Creo que fue m�s como una epifan�a.
Sal�a John Cage.

18
00:02:32,956 --> 00:02:36,949
No creas que estoy loca,
pero creo que es el definitivo.

19
00:02:37,156 --> 00:02:39,716
John Cage y yo,. . .

20
00:02:39,916 --> 00:02:41,952
. . .�l es el definitivo.

21
00:02:51,916 --> 00:02:53,793
Amigos para siempre

22
00:04:04,436 --> 00:04:10,193
- Bizcochito te est� buscando.
- �S�? �Dijo qu� quer�a?

23
00:04:10,396 --> 00:04:12,387
�Qu� llevaba puesto?

24
00:04:12,636 --> 00:04:16,151
Ally, un caso nuevo. Necesito que
vayas de segunda abogada.

25
00:04:20,596 --> 00:04:23,793
- �Todav�a sigue haciendo eso?
- Me encanta.

26
00:04:24,036 --> 00:04:26,994
- Billy.
- Perdona.

27
00:04:27,396 --> 00:04:29,068
Quietas.

28
00:04:31,956 --> 00:04:34,754
�Quietas?
�Te siguen y te esperan?

29
00:04:34,956 --> 00:04:36,947
- � Y qu� m�s. . .?
- Richard.

30
00:04:38,316 --> 00:04:40,955
Tengo una reuni�n
con el jefe de Bell.

31
00:04:41,156 --> 00:04:44,944
�Otro Neanderthal que responde
a los aplausos de tontitas?

32
00:04:45,156 --> 00:04:47,545
- Es un estilo.
- S�, es un estilo. . .

33
00:04:47,756 --> 00:04:50,190
Tengo una ligera oportunidad
de gan�rmelo.

34
00:04:50,436 --> 00:04:53,985
Hablamos de una compa��a
valorada en m�s de 500$ millones.

35
00:04:54,196 --> 00:04:56,756
� Y rodearse de chicas ayuda?

36
00:04:56,956 --> 00:04:59,754
<i>- Pues me rodeo con chicas.
- Porque:</i>

37
00:04:59,996 --> 00:05:03,511
<i>Hay un nuevo hombre en la ciudad</i>

38
00:05:04,556 --> 00:05:06,353
Exactamente.

39
00:05:12,516 --> 00:05:14,268
�John? �Me quer�as?

40
00:05:15,476 --> 00:05:18,513
Estaba buscando el horario de juicios
y lo encontr�.

41
00:05:19,156 --> 00:05:20,748
Est� bien.

42
00:05:22,596 --> 00:05:24,712
Bueno, �c�mo te va?

43
00:05:26,356 --> 00:05:28,153
Bien. �Por qu�?

44
00:05:28,756 --> 00:05:31,031
Hace tiempo que no hablamos.

45
00:05:31,196 --> 00:05:34,233
Me gustar�a ponerme al d�a.

46
00:05:35,156 --> 00:05:39,946
- �Te est�s muriendo?
- No. No me estoy muriendo. �Por qu�?

47
00:05:40,156 --> 00:05:43,944
�Si me interesa la vida de alguien
es que me debo estar muriendo?

48
00:05:44,156 --> 00:05:45,748
S�.

49
00:05:45,996 --> 00:05:50,626
S�lo quer�a saber c�mo le iba
a un amigo. Eso es todo, colega.

50
00:05:50,836 --> 00:05:52,633
- �Est�s enferma?
- Olv�dalo.

51
00:05:59,796 --> 00:06:01,832
�Ally? �Qu� ocurre?

52
00:06:03,396 --> 00:06:05,512
Nada. Estoy bien.

53
00:06:05,996 --> 00:06:07,748
�Est�s segura?

54
00:06:08,596 --> 00:06:10,348
S�.

55
00:06:11,756 --> 00:06:14,111
�Por qu� te est�s quitando tus. . .?

56
00:06:14,996 --> 00:06:18,227
Tengo una posible cita.
Un amigo de un amigo.

57
00:06:18,436 --> 00:06:21,746
Est� aqu�.
Por tel�fono sonaba incre�ble.

58
00:06:21,956 --> 00:06:26,552
Son los pantalones para emergencias.
Est�n impregnados con feromonas.

59
00:06:27,756 --> 00:06:31,544
�Tienes pantalones
impregnados con feromonas?

60
00:06:31,996 --> 00:06:38,151
No lo puedes oler. Pero funciona.
Conf�a en m�, est� estudiado.

61
00:06:38,356 --> 00:06:40,506
- �C�mo estoy?
- Bien.

62
00:06:40,956 --> 00:06:44,744
Es dif�cil dar con el hombre adecuado.
Hay que poner empe�o.

63
00:06:44,956 --> 00:06:47,345
- Ya est�.
- �Elaine?

64
00:06:47,596 --> 00:06:52,590
�Alguna vez has conocido a alguien
que pensases que era ideal para m�?

65
00:06:53,316 --> 00:06:55,466
- No.
- �A nadie?

66
00:06:56,116 --> 00:06:58,949
No es que no seas una gran persona,...

67
00:06:59,156 --> 00:07:02,193
...es s�lo que eres tan complicada.

68
00:07:02,436 --> 00:07:06,952
La �nica persona ser�a John.
Pero no quieres nada con �l.

69
00:07:07,156 --> 00:07:08,635
�No?

70
00:07:09,356 --> 00:07:11,472
- �Por qu�?
- �Quieres algo con �l?

71
00:07:11,996 --> 00:07:13,714
No.

72
00:07:13,916 --> 00:07:16,384
Est� con Nelle. Sobra la pregunta.

73
00:07:16,596 --> 00:07:19,064
Pero, �por qu� crees
que nosotros...?

74
00:07:19,276 --> 00:07:23,394
�No es obvio? Los dos tienen
ese mismo mundo interior.

75
00:07:23,556 --> 00:07:28,391
La entiendes mejor que Nelle. Y �l
a ti mejor de lo que lo hizo Billy.

76
00:07:31,796 --> 00:07:33,309
�Est�s bien?

77
00:07:34,236 --> 00:07:36,989
S�. Perdona.

78
00:07:37,316 --> 00:07:38,954
Perdona.

79
00:07:43,036 --> 00:07:46,711
John. Hola. �Qu� tal?

80
00:07:46,916 --> 00:07:50,511
Todav�a bien. Sin cambios
despu�s de dos minutos.

81
00:07:53,036 --> 00:07:58,064
Qu� gracioso.
Me encanta tu sentido del humor.

82
00:07:59,356 --> 00:08:01,586
Bueno, �qu� pasa?

83
00:08:01,836 --> 00:08:05,875
S�lo me estoy ajustando
al nuevo milenio.

84
00:08:35,156 --> 00:08:37,272
- �Elaine?
- �Bob?

85
00:08:37,476 --> 00:08:41,594
- Hola. Encantado de conocerte.
- lgualmente.

86
00:08:41,836 --> 00:08:45,511
Sonabas bien por tel�fono.
Est�s mejor en persona.

87
00:08:45,676 --> 00:08:49,589
Gracias. T� tambi�n.
Bien, �nos vamos?

88
00:08:52,276 --> 00:08:55,393
Mira, ment� sobre lo de querer
ir a comer.

89
00:08:55,556 --> 00:08:59,185
Es m�s seguro que salir
de noche con un desconocido.

90
00:08:59,436 --> 00:09:02,985
Ahora que nos conocemos, olvida
la comida. Arreglemos para cenar.

91
00:09:03,156 --> 00:09:04,191
Por m� bien.

92
00:09:04,396 --> 00:09:08,355
- Bien, a las 8. P�same a buscar.
- Qu� f�cil.

93
00:09:08,556 --> 00:09:10,592
- Hasta luego.
- Hasta luego.

94
00:09:10,836 --> 00:09:14,306
- Y una vez m�s, encantado.
- lgualmente.

95
00:09:20,956 --> 00:09:22,514
Has estado muy h�bil.

96
00:09:23,156 --> 00:09:27,513
Si quieres conseguir al Sr. ldeal,
tienes que hacer que ocurra.

97
00:09:33,356 --> 00:09:36,154
- �Has o�do eso?
- �El qu�?

98
00:09:36,836 --> 00:09:38,554
Nada.

99
00:09:39,116 --> 00:09:40,947
Nada.

100
00:09:47,316 --> 00:09:51,150
Cuando algo ocurre,
generalmente nos lo contamos.

101
00:09:59,716 --> 00:10:00,705
�Alguna vez. . .?

102
00:10:05,156 --> 00:10:10,105
John, si conociese a un tipo
que podr�a ser el definitivo,. . .

103
00:10:11,356 --> 00:10:16,350
. . .pero que estuviese con otra
persona. Cu�nto, no lo s�.

104
00:10:16,596 --> 00:10:19,315
- Pero que podr�a ser �l.
- A m� me pas�.

105
00:10:19,956 --> 00:10:22,345
- �S�?
- Una mujer. . .

106
00:10:22,596 --> 00:10:25,747
Estaba convencido de que ella y yo. . .
Pero ella a�n. . .

107
00:10:25,996 --> 00:10:31,150
No estaba saliendo con su antiguo
novio, pero algo pasaba.

108
00:10:31,396 --> 00:10:34,194
�l era mi amigo. Pero ella. . .

109
00:10:34,436 --> 00:10:38,588
- Nunca he conocido a nadie como ella.
- � Y qu� hiciste?

110
00:10:38,996 --> 00:10:43,308
- La invit� a salir. �C�mo evitarlo?
- � Y qu� pas�?

111
00:10:43,716 --> 00:10:47,391
La cita fue un desastre.
Al menos lo intent�.

112
00:10:47,636 --> 00:10:50,548
No me lo habr�a perdonado
si no lo hubiese hecho.

113
00:10:52,356 --> 00:10:56,190
- �Fue un desastre?
- Total. Pero seguimos siendo amigos.

114
00:10:56,436 --> 00:11:00,349
- De hecho, creo que somos muy amigos.
- �La conozco?

115
00:11:00,556 --> 00:11:02,467
Algunos d�as, s�. Otros, no.

116
00:11:03,756 --> 00:11:05,951
�Qu� significa eso?

117
00:11:06,796 --> 00:11:10,755
- Ally, eres t�.
- � Yo?

118
00:11:12,996 --> 00:11:15,556
S�, qu� curioso, �eh?

119
00:11:17,156 --> 00:11:18,953
S�.

120
00:11:40,356 --> 00:11:42,153
�Qu� pasa?

121
00:11:42,956 --> 00:11:46,915
- Nada. �Por qu�?
- Te quedas clavada ante el espejo.

122
00:11:47,116 --> 00:11:49,676
Tal vez s�lo me estaba mirando.

123
00:11:49,876 --> 00:11:52,709
Est� bien. No me lo digas.

124
00:11:54,236 --> 00:11:58,149
Est� bien. Est� bien, te lo dir�.

125
00:11:58,876 --> 00:12:03,586
Conozco a un hombre. De hecho,
lo conozco desde hace mucho tiempo.

126
00:12:03,836 --> 00:12:06,714
Y nada, creo que ser�a perfecto
para m�.

127
00:12:07,836 --> 00:12:11,875
La cosa es...que tiene novia.

128
00:12:12,076 --> 00:12:14,544
Los mejores hombres
siempre est�n ocupados.

129
00:12:14,796 --> 00:12:17,993
Si no los robas, no los consigues.

130
00:12:18,236 --> 00:12:22,946
�Estamos hablando de una cita
divertida, o del hombre de tu vida?

131
00:12:23,156 --> 00:12:25,545
- El definitivo.
- El �ltimo. El definitivo.

132
00:12:25,756 --> 00:12:29,669
� Y te aguantas por cortes�a
hacia otra mujer?

133
00:12:29,876 --> 00:12:33,994
Normalmente no se conoce a ese hombre,
y menos, se tiene la oportunidad. . .

134
00:12:34,156 --> 00:12:38,069
� Y si no funciona?
Estropear� su relaci�n con su novia.

135
00:12:38,276 --> 00:12:42,792
- �Te preocupas por �l?
- Bueno, s�.

136
00:12:43,156 --> 00:12:47,547
Pues olv�dalo.
No creo que debas acercarte a �l.

137
00:12:47,796 --> 00:12:52,153
Gracias, Ling.
Has sido de gran ayuda.

138
00:13:14,196 --> 00:13:16,232
- Eh.
- �Qu� es eso?

139
00:13:18,596 --> 00:13:20,951
- Zapatos.
- Oh, no.

140
00:13:21,356 --> 00:13:25,349
- �Un fetiche con los zapatos?
- No tengo. . .

141
00:13:25,596 --> 00:13:28,633
- Primero los golpes...
- No tengo ning�n fetiche.

142
00:13:28,836 --> 00:13:34,752
- �Para qui�n son los zapatos?
- Bueno, si lo quieres saber, para ti.

143
00:13:36,716 --> 00:13:40,345
- �Para m�?
- S�, p�ntelos.

144
00:13:40,556 --> 00:13:44,834
�Qu� bueno! Son preciosos.

145
00:13:47,396 --> 00:13:49,956
Me encantan.

146
00:13:50,956 --> 00:13:52,753
F�jate.

147
00:13:55,716 --> 00:13:58,150
Para eso sirven.

148
00:14:01,276 --> 00:14:06,953
- �C�mo que dejas el trabajo?
- No est� saliendo como pensaba.

149
00:14:07,156 --> 00:14:09,989
- �Por qu�?
- Porque no. Te lo agradezco. . .

150
00:14:10,196 --> 00:14:14,553
Sandy, creo que al menos
me debes una explicaci�n.

151
00:14:15,036 --> 00:14:17,470
Est� bien. Creo que eres un idiota.

152
00:14:18,876 --> 00:14:20,229
No me malinterpretes.

153
00:14:20,476 --> 00:14:23,707
Eres un tipo maravilloso
bajo la capa de un idiota.

154
00:14:23,916 --> 00:14:26,146
�Ha pasado algo?

155
00:14:26,356 --> 00:14:28,995
Soy secretaria de abogado.
Es un buen trabajo.

156
00:14:29,236 --> 00:14:33,593
Una buena carrera. Me lo tomo en serio
y quiero que los dem�s tambi�n.

157
00:14:33,836 --> 00:14:39,627
Es de tontos que mi jefe ande por
ah� con las chicas de Robert Palmer.

158
00:14:40,156 --> 00:14:44,354
Te hace quedar como un tonto,
y creo que me hace parecer tonta.

159
00:14:48,316 --> 00:14:53,151
Lo siento. Nunca quise humillarte.

160
00:14:53,356 --> 00:14:58,146
Lo s�. Y el nuevo estilo funciona,
as� que es mi problema, no el tuyo.

161
00:14:58,956 --> 00:15:02,187
No quiero que te vayas.
Me deshar� de las chicas.

162
00:15:02,436 --> 00:15:05,394
Soy una ayudante.
No deber�a decirte. . .

163
00:15:05,636 --> 00:15:09,026
Me librar� de las chicas.
Quiero que te quedes.

164
00:15:17,676 --> 00:15:20,986
No parecen muy llamativas, �no?

165
00:15:21,236 --> 00:15:27,550
De hecho, necesitan parecer
m�s vivas y despiertas.

166
00:15:27,796 --> 00:15:28,945
Eso es.

167
00:15:29,956 --> 00:15:31,947
Bueno, la verdad. . .

168
00:15:33,436 --> 00:15:36,508
Ya est�. �Qu� piensas?
�C�mo est�n sus pechos?

169
00:15:38,396 --> 00:15:40,387
S�, es. . .

170
00:15:41,596 --> 00:15:45,191
�John! �Qu� tal?
Han pasado m�s de dos minutos.

171
00:15:45,436 --> 00:15:47,950
�Seguro que todav�a
todo est� bien!

172
00:16:07,716 --> 00:16:09,149
�No es f�cil!

173
00:16:12,236 --> 00:16:14,352
Ally, te est�s comportando tan. . .

174
00:16:14,596 --> 00:16:18,145
�Est�s nerviosa por
ese dilema que tienes?

175
00:16:24,836 --> 00:16:26,349
Yo. . .

176
00:16:27,956 --> 00:16:30,231
�El hombre del que hablaba?

177
00:16:32,756 --> 00:16:35,224
Anoche so�� contigo. . .

178
00:16:36,556 --> 00:16:39,116
. . .y conmigo. Juntos,. . .

179
00:16:40,516 --> 00:16:42,347
. . .como pareja.

180
00:16:42,556 --> 00:16:47,949
<i>Est�bamos en el ascensor
y t� me dijiste algo como:</i>

181
00:16:48,156 --> 00:16:52,149
"T� y yo estamos cada d�a,
cara a cara" .

182
00:16:52,396 --> 00:16:57,629
Y entonces me di cuenta.
Y despu�s me despert�. . .

183
00:16:57,836 --> 00:16:59,906
. . .y segu� d�ndome cuenta, John.

184
00:17:00,356 --> 00:17:03,712
El que t� puedas ver
dentro de mi vida imaginaria,. . .

185
00:17:03,916 --> 00:17:05,986
... el que yo oiga a Barry White.

186
00:17:06,236 --> 00:17:10,149
Hace unas semanas, te describ�as
a ti mismo como un solitario.

187
00:17:10,396 --> 00:17:15,345
Y dec�as algo sobre
c�mo te saqu� al exterior.

188
00:17:15,556 --> 00:17:18,548
Y me di cuenta. . .

189
00:17:19,196 --> 00:17:21,756
...de que t� me sacaste
al exterior.

190
00:17:21,956 --> 00:17:25,392
Hay muchas partes de m�,
sobre las que me averg�enzo. . .

191
00:17:25,636 --> 00:17:29,265
. . .o de las que tengo miedo
de compartir con otros.

192
00:17:31,556 --> 00:17:37,711
Estoy a gusto comparti�ndolas
contigo. No lo puedo evitar.

193
00:17:37,916 --> 00:17:40,953
lncluso tengo a las malditas Ikettes
cant�ndome. . .

194
00:17:41,156 --> 00:17:44,626
. . .lo f�cil que es enamorarse.

195
00:17:46,836 --> 00:17:48,554
No lo puedo evitar.

196
00:17:50,196 --> 00:17:54,553
Y luego, cuando dijiste
lo que dijiste. . .

197
00:18:19,996 --> 00:18:22,385
Nelle. Hola.

198
00:18:23,396 --> 00:18:25,352
Hola.

199
00:18:27,956 --> 00:18:30,151
�Has visto ad�nde fue John?

200
00:18:30,596 --> 00:18:34,350
Ten�a prisa.
Dijo que me ver�a en el bar.

201
00:18:36,956 --> 00:18:40,312
Oh, el bar.
Es el momento, creo.

202
00:18:45,556 --> 00:18:47,069
Creo.

203
00:19:22,156 --> 00:19:25,353
- Llegas tarde. �D�nde has estado?
- Ten�a chicos. . .

204
00:19:25,596 --> 00:19:27,029
Cosas que hacer. Trabajo.

205
00:19:27,356 --> 00:19:30,393
- �Todo bien?
- S�.

206
00:19:31,156 --> 00:19:36,150
- �Has hablado con Bizcochito?
- Acabo de hablar con �l. Se escap�.

207
00:19:36,396 --> 00:19:39,786
- Lo has asustado.
- Fue algo ego�sta de mi parte.

208
00:19:40,036 --> 00:19:42,596
A �l le va bien con Nelle.

209
00:19:42,836 --> 00:19:45,873
Oh, por favor.
�C�mo le va a ir bien. . .

210
00:19:46,116 --> 00:19:49,995
. . .con esa bruja rubia y cre�da?

211
00:19:50,236 --> 00:19:53,945
Renee, �por qu� no nos dices
c�mo te sientes en realidad?

212
00:20:07,156 --> 00:20:09,147
Mi casa es por ah�.

213
00:20:09,356 --> 00:20:12,792
- Lo he pasado realmente bien.
- Yo tambi�n, de verdad.

214
00:20:13,036 --> 00:20:17,552
- Eres muy astuta, Elaine.
- Te he dado mi n�mero, �no?

215
00:20:17,756 --> 00:20:22,352
S�, el de casa, el m�vil, el busca
y tu e-mail. �Cu�l es �ste?

216
00:20:22,596 --> 00:20:27,351
Mi instituto. Saben c�mo localizarme
para pedirme dinero.

217
00:20:28,756 --> 00:20:31,429
Est� bien, buenas noches.

218
00:20:32,036 --> 00:20:34,550
Vamos, la noche reci�n empieza.

219
00:20:34,796 --> 00:20:37,629
Son las doce menos cuarto
de un lunes.

220
00:20:38,156 --> 00:20:42,354
Ya sabes, el vino.
Deber�a tomar un caf�. . .

221
00:20:42,596 --> 00:20:46,987
- . . .antes de volver a casa.
- Est�s a tres manzanas.

222
00:20:47,596 --> 00:20:49,154
Yo. . .

223
00:20:49,756 --> 00:20:52,793
- Te deseo.
- Qu� tierno.

224
00:20:56,316 --> 00:20:58,147
Bob.

225
00:20:59,676 --> 00:21:03,988
- Pensaba que yo te gustaba.
- S�, pero es la primera cita.

226
00:21:04,876 --> 00:21:07,834
Con Danny O'Connell,
en la primera cita, t�. . .

227
00:21:09,636 --> 00:21:15,472
- �Danny te cont� nuestra 1 � cita?
- No. S�lo. . .

228
00:21:15,676 --> 00:21:18,110
Lo que pas� despu�s de la cita.

229
00:21:19,396 --> 00:21:22,752
�El amigo que te dio mi numero
fue Danny?

230
00:21:23,556 --> 00:21:25,353
S�.

231
00:21:27,796 --> 00:21:29,548
Bueno,. . .

232
00:21:30,196 --> 00:21:33,347
- . . .buenas noches.
- Mira, �puedo preguntarte algo?

233
00:21:33,956 --> 00:21:36,186
Parece que nos entend�amos.

234
00:21:36,396 --> 00:21:40,947
De verdad. Danny dijo que lo pasaste
bien, pero no fue. . .

235
00:21:41,156 --> 00:21:43,989
�Por qu� le invitaste a subir a �l
y a m� no?

236
00:21:44,196 --> 00:21:49,748
Porque t� y yo nos entendemos
de verdad. Pensaba que quiz�s. . .

237
00:21:49,956 --> 00:21:52,345
- No importa.
- Seg�n lo que oigo,. . .

238
00:21:52,556 --> 00:21:57,027
. . .te gusto m�s que Danny,
pero me castigas por eso.

239
00:21:57,236 --> 00:22:01,991
- Deber�a subir.
- Buenas noches, Bob.

240
00:22:22,996 --> 00:22:24,349
�Billy?

241
00:22:25,316 --> 00:22:30,344
- Hoy est�s como que te falta algo.
- No tengo reuniones hoy. �Algo m�s?

242
00:22:30,596 --> 00:22:32,188
Perfecto, adi�s.

243
00:22:33,916 --> 00:22:37,147
Se ha cancelado el caso de Talbot.
Van a pactar.

244
00:22:37,316 --> 00:22:40,149
El juez ha programado una sesi�n.

245
00:22:41,236 --> 00:22:43,989
- Fant�stico.
- Pens� que te alegrar�as.

246
00:22:44,236 --> 00:22:45,589
- �Qu� ocurre?
- Nada.

247
00:22:45,916 --> 00:22:47,508
Elaine. . .

248
00:22:48,116 --> 00:22:51,313
- �C�mo fue la cita?
- Nos llevamos de mil maravillas.

249
00:22:51,556 --> 00:22:54,992
Se port� como un salm�n que va
r�o arriba para soltar sus huevos.

250
00:22:59,276 --> 00:23:02,074
Vuelvo en un segundo.

251
00:23:03,956 --> 00:23:05,230
John.

252
00:23:05,436 --> 00:23:06,994
Hola, cari�o.

253
00:23:11,276 --> 00:23:12,789
- �Todo bien?
- S�.

254
00:23:13,036 --> 00:23:16,392
Richard, necesito hablar contigo.
Es importante.

255
00:23:18,836 --> 00:23:24,229
- �La gente piensa que soy promiscua?
- En cierto modo es como te anuncias.

256
00:23:26,276 --> 00:23:28,392
- �Qu� pas�?
- Nada.

257
00:23:28,676 --> 00:23:30,394
�Qu� pas�?

258
00:23:31,396 --> 00:23:33,114
Bueno, la cita fue genial.

259
00:23:33,356 --> 00:23:37,668
Es el primer tipo con el que he estado
en mucho tiempo y he pensado. . .

260
00:23:37,916 --> 00:23:39,668
- �Que era un posible?
- S�.

261
00:23:40,036 --> 00:23:44,427
Sali� conmigo porque alguien le dijo
que seguro que me acostaba con �l.

262
00:23:44,636 --> 00:23:49,994
- Es un asqueroso. No un posible.
- S�.

263
00:23:51,076 --> 00:23:54,432
- Elaine. . .
- Tienes raz�n. Es un asqueroso.

264
00:23:54,676 --> 00:23:56,985
No me deber�a molestar.

265
00:24:01,276 --> 00:24:03,107
- �Ella dijo eso?
- �S�!

266
00:24:03,356 --> 00:24:06,314
- As� es que tuvo un sue�o. . .
- No, no es el sue�o.

267
00:24:06,556 --> 00:24:10,390
Me sugiri� la posibilidad de que
explor�semos ser pareja.

268
00:24:10,596 --> 00:24:13,986
He de admitir que
siento algo por Ally.

269
00:24:14,436 --> 00:24:17,872
Vaya noticia.
Hay consideraciones �ticas.

270
00:24:18,076 --> 00:24:22,627
<i>La pregunta es: �puedes dormir
con Ally sin que Nelle lo descubra?</i>

271
00:24:22,836 --> 00:24:25,908
�sa no es la pregunta.
Amo a Nelle. En serio.

272
00:24:26,116 --> 00:24:27,549
Pero Ally. . .

273
00:24:27,876 --> 00:24:32,233
Me alucina mi conexi�n con ella.
Podr�a ir mucho m�s all�.

274
00:24:32,396 --> 00:24:34,114
Es una corazonada.

275
00:24:34,276 --> 00:24:38,827
Y el riesgo de explorarlo, debido a
Nelle. �Entiendes lo que digo?

276
00:24:39,076 --> 00:24:42,830
- Te quieres acostar con las dos.
- Vamos, hombre, en serio.

277
00:24:43,076 --> 00:24:44,555
John,. . .

278
00:24:45,956 --> 00:24:48,311
. . .�a qui�n quieres m�s?

279
00:24:48,476 --> 00:24:52,469
No lo s�. No he explorado nunca
una relaci�n con Ally.

280
00:24:52,636 --> 00:24:55,309
Todav�a no he explorado
a fondo a Nelle.

281
00:24:55,596 --> 00:24:57,985
Explora a Nelle. Despu�s a Ally. . .

282
00:24:58,196 --> 00:25:01,905
Qu� simple es todo para ti, �no?
�Sexo, sexo, sexo!

283
00:25:02,276 --> 00:25:07,987
El cuello de Whipper contra la rodilla
de Ling. Fish superficial.

284
00:25:08,156 --> 00:25:09,589
�Sexo, sexo!

285
00:25:10,676 --> 00:25:12,792
Fish superficial.

286
00:25:12,996 --> 00:25:14,588
Me gusta eso.

287
00:25:22,876 --> 00:25:25,344
Georgia, �eh! Te extran�bamos.

288
00:25:26,476 --> 00:25:27,989
Est� bien.

289
00:25:29,236 --> 00:25:31,466
Georgia, hola.

290
00:25:31,716 --> 00:25:33,672
Hola. Esto es para ti.

291
00:25:38,756 --> 00:25:40,633
�Solicitas el divorcio?

292
00:25:40,796 --> 00:25:45,233
Renee me representa. Fija un abogado
para que puedan reunirse.

293
00:25:45,436 --> 00:25:47,347
- Divorcio, �as� de f�cil?
- S�.

294
00:25:47,556 --> 00:25:50,866
- � Y me das t� los papeles en mano?
- S�.

295
00:26:05,676 --> 00:26:07,394
�Puedo hacer algo?

296
00:26:08,276 --> 00:26:09,789
No.

297
00:26:10,636 --> 00:26:12,228
Lo siento.

298
00:26:13,676 --> 00:26:14,870
Ya.

299
00:26:22,956 --> 00:26:24,628
Me siento horrible.

300
00:26:24,836 --> 00:26:29,068
Ally, para conseguir lo que mereces,
tienes que ir decidida.

301
00:26:29,276 --> 00:26:32,871
- Y me merezco lo que me merezco, �no?
- Por supuesto.

302
00:26:33,076 --> 00:26:36,193
- Deber�a conseguir lo que me merezco.
- Muy bien.

303
00:26:47,716 --> 00:26:50,071
Ling, �eh! �Qu� pasa?

304
00:26:50,476 --> 00:26:53,991
- �Por qu� no me lo dices, Ally?
- �Perdona?

305
00:26:54,196 --> 00:26:59,190
Me lo imaginaba. Este amor de tu vida
es el graciosito, el bajito.

306
00:27:00,036 --> 00:27:02,072
- �Qui�n?
- Nelle es mi mejor amiga.

307
00:27:02,276 --> 00:27:04,392
Si crees que dejar� que le robes. . .

308
00:27:04,596 --> 00:27:07,827
- No sabes ni si te gustan los hombres.
- �S� que me gustan!

309
00:27:08,036 --> 00:27:11,665
- Dame esa pierna.
- No, no voy a darte mi pierna.

310
00:27:12,356 --> 00:27:13,709
Dios. . .

311
00:27:15,356 --> 00:27:16,584
Para.

312
00:27:19,876 --> 00:27:22,390
�D�nde aprendiste a. . .?
No importa.

313
00:27:22,596 --> 00:27:26,794
- Ally, �no hagas eso!
- �No he hecho nada! �Vete!

314
00:27:35,556 --> 00:27:38,787
Para un caso de divorcio,
Nelle es tu mejor opci�n.

315
00:27:39,036 --> 00:27:43,109
- Quiero un equipo.
- Que as� sea. Estamos contigo.

316
00:27:43,276 --> 00:27:48,111
�Elaine! Me encanta tu nuevo aspecto.
�Se llevan las monjas hoy en d�a?

317
00:27:49,676 --> 00:27:51,587
Eh, Richard, vete al infierno.

318
00:27:54,396 --> 00:27:58,628
- �Estaba enfadada o era broma?
- No tengo ni idea.

319
00:27:58,836 --> 00:28:02,590
Mujeres. Creo que est�n todas locas, Billy.

320
00:28:12,276 --> 00:28:15,427
�Elaine? �Te ha molestado algo
que haya dicho?

321
00:28:15,676 --> 00:28:19,191
No, no es nada importante.

322
00:28:19,436 --> 00:28:24,635
�Hay algo de lo que quieras hablar?
Puedo llamar a Ally o a John.

323
00:28:27,556 --> 00:28:31,993
Richard, si pudieses describirme
con una palabra, �cual ser�a?

324
00:28:32,876 --> 00:28:35,470
Bueno...rubia.

325
00:28:36,076 --> 00:28:40,627
- � Y un adjetivo para mi personalidad?
- Ah, entiendo. Divertida.

326
00:28:41,236 --> 00:28:43,830
- Esa ser�a. �Divertida?
- lnteligente.

327
00:28:44,596 --> 00:28:50,626
Si un amigo tuyo fuera a salir
conmigo, �qu� palabra utilizar�as?

328
00:28:51,156 --> 00:28:52,828
�sa es f�cil.

329
00:28:53,556 --> 00:28:57,515
- Est� bien, �cu�l ser�a?
- F�cil.

330
00:28:59,276 --> 00:29:00,629
Gracias.

331
00:29:01,756 --> 00:29:06,227
- Elaine. . .
- Por eso salen los hombres conmigo.

332
00:29:06,476 --> 00:29:09,434
Porque soy f�cil.
Porque creen que soy f�cil.

333
00:29:09,996 --> 00:29:14,228
Bueno, pues no lo soy. Pasa la
palabra, maldita sea. No soy f�cil.

334
00:29:14,436 --> 00:29:18,588
Pero si lo dices a gritos, Elaine.
Me encantan las perras.

335
00:29:21,476 --> 00:29:24,786
Genial. Ahora es ella la que
atraviesa un cambio.

336
00:29:38,276 --> 00:29:40,267
- �John?
- Oh, Ally.

337
00:29:40,516 --> 00:29:43,986
Hola. Ten�a puntadas en la cabeza.

338
00:29:45,076 --> 00:29:48,955
Lo siento mucho.

339
00:29:51,396 --> 00:29:55,309
Quiero que sepas que te respeto a ti
y a Nelle como pareja.

340
00:29:55,516 --> 00:29:58,394
Y que nunca har�a nada para. . .

341
00:30:00,596 --> 00:30:04,066
Fue realmente injusto por mi parte
hacer lo que hice.

342
00:30:04,276 --> 00:30:09,828
- �Hacer qu�? �Ser sincera?
- A veces la sinceridad es ego�sta.

343
00:30:10,076 --> 00:30:13,432
No me hubiese molestado si yo no. . .

344
00:30:14,036 --> 00:30:15,594
�Si no qu�?

345
00:30:17,156 --> 00:30:21,593
Es algo que no hemos mencionado
desde hace mucho tiempo.

346
00:30:21,756 --> 00:30:26,705
Hablamos sobre ser almas gemelas
y el hecho es que no encajamos.

347
00:30:27,196 --> 00:30:31,508
C�mo aceptamos ser raros, que los dos
tenemos mundos interiores.

348
00:30:31,876 --> 00:30:35,107
Todas esas cosas peculiares
que tenemos en com�n.

349
00:30:35,316 --> 00:30:39,832
A un cierto nivel, nos preguntamos
si somos el uno para el otro.

350
00:30:41,676 --> 00:30:45,191
No has tartamudeado ni una sola vez
para decir eso.

351
00:30:45,396 --> 00:30:50,390
He visto a la Dra. Flott. Me ayuda a
centrarme en lo que quiero en la vida.

352
00:30:50,836 --> 00:30:55,068
Cuando me siento enfrente de ella,
me inunda esta incre�ble claridad.

353
00:30:55,236 --> 00:31:00,071
Las locuras que t� y yo compartimos,
la conexi�n de las vidas internas.

354
00:31:00,436 --> 00:31:03,314
- Yo s� lo que quiero.
- �S�?

355
00:31:04,356 --> 00:31:08,986
Quiero a Nelle como novia
y a ti como terapeuta.

356
00:31:12,276 --> 00:31:15,393
Bueno, vaya. . .

357
00:31:15,636 --> 00:31:20,391
La idea de poner en peligro
nuestra amistad para ser pareja. . .

358
00:31:20,636 --> 00:31:26,427
- Una locura de la que no soy capaz.
- S�.

359
00:31:26,676 --> 00:31:28,871
- Estupendo.
- S�.

360
00:31:29,236 --> 00:31:30,430
Bueno,. . .

361
00:31:31,076 --> 00:31:33,670
- . . .�amigos para siempre?
- S�.

362
00:31:50,596 --> 00:31:54,714
Elaine, pareces la Srta. Hathaway.

363
00:31:54,876 --> 00:31:57,834
Si Billy puede cambiar, yo tambi�n.

364
00:31:58,036 --> 00:32:00,231
Hola. Buenos d�as, chicas.

365
00:32:02,116 --> 00:32:05,347
El que diga buenos d�as,
no significa que lo sean.

366
00:32:06,396 --> 00:32:09,832
Nelle, ya que est�s aqu�,
y s�lo por curiosidad,. . .

367
00:32:09,996 --> 00:32:14,387
- . . .�cu�nto me respetan las mujeres?
- No creo que lo hagan, Elaine.

368
00:32:19,556 --> 00:32:22,992
- Eso ha estado bien.
- �D�nde est� el sentido del humor?

369
00:32:23,196 --> 00:32:26,154
�La vida no es divertida!
�No te das cuenta?

370
00:32:38,836 --> 00:32:42,385
�Ally! Todav�a le est�s dando vueltas.
D�jalo.

371
00:32:50,836 --> 00:32:55,591
- �Ally! Buenos d�as.
- � Ya estamos con lo de "buenos d�as"?

372
00:32:56,036 --> 00:32:59,870
- � Va todo bien?
- Te he acosado como a un paciente. . .

373
00:33:00,036 --> 00:33:03,585
... y las cosas no podr�an ir mejor.
� Verdad, John?

374
00:33:04,796 --> 00:33:08,186
Obviamente est�s molesta
con mi decisi�n.

375
00:33:11,356 --> 00:33:15,508
No, creo que, de hecho,
tomaste la decisi�n correcta.

376
00:33:15,716 --> 00:33:18,992
Sabes lo mucho que te adoro
y te admiro.

377
00:33:19,276 --> 00:33:20,914
S�, s�.

378
00:33:22,236 --> 00:33:24,067
Tengo curiosidad, John.

379
00:33:24,516 --> 00:33:29,112
�C�mo te lleg� esa claridad?

380
00:33:29,596 --> 00:33:34,624
Bueno, afront�moslo. Soy raro.
Tengo todas esas excentricidades.

381
00:33:34,996 --> 00:33:37,908
Y, bueno, lo admito.
Soy un bicho raro.

382
00:33:38,236 --> 00:33:42,275
Como pareja, necesito a alguien que
me ponga los pies sobre la tierra.

383
00:33:42,436 --> 00:33:46,315
- Y t� est�s m�s falta de juicio que yo.
- � Yo, falta de juicio?

384
00:33:46,556 --> 00:33:50,151
- Te lo digo con cari�o.
- T� est�s m�s falto de juicio que nadie.

385
00:33:50,396 --> 00:33:54,389
- No te pongas en ese plan.
- �Qui�n ve a Barry en el espejo?

386
00:33:54,596 --> 00:33:57,508
Adem�s de Barry.
As� que no me llames chiflada.

387
00:33:57,716 --> 00:34:00,594
Yo s� que no est� ah�.
�Pero t� y Al Green?

388
00:34:00,756 --> 00:34:03,509
- En cuanto a. . .
- � Y aquella cancioncita?

389
00:34:03,716 --> 00:34:08,426
- �Has visto a alg�n beb� �ltimamente?
- �Sabes qu�? Tus pies huelen ma�-

390
00:34:08,676 --> 00:34:11,349
No te lo decimos porque
te picar�a la nariz.

391
00:34:11,556 --> 00:34:14,354
Qu� bien. Te rechazo y
mira c�mo te pones.

392
00:34:14,556 --> 00:34:18,151
Sal de aqu� tartamudeando,
�graciosito, bajito!

393
00:34:21,436 --> 00:34:23,950
La terapia de la risa.
�Est�s chiflada!

394
00:34:32,996 --> 00:34:34,429
�Elaine?

395
00:34:36,196 --> 00:34:37,948
Lo siento.

396
00:34:39,156 --> 00:34:41,829
Lo pas� genial.
Creo que t� tambi�n.

397
00:34:42,236 --> 00:34:44,989
�Por qu� dejarlo s�lo porque. . .?

398
00:34:45,156 --> 00:34:48,228
�Puedo intentarlo de nuevo?
Ser� un perfecto caballero.

399
00:34:48,396 --> 00:34:52,275
No eres un caballero.
Se es o no se es.

400
00:34:52,476 --> 00:34:56,435
- No voy a salir contigo otra vez.
- Yo no soy el malo.

401
00:34:56,836 --> 00:35:00,465
Salimos juntos por lo que
pens�bamos de cada uno.

402
00:35:00,716 --> 00:35:03,913
Y est�bamos equivocados. Ahora vete.

403
00:35:25,196 --> 00:35:27,756
Una vez m�s, lo siento.

404
00:35:29,156 --> 00:35:31,590
- S� que ten�as raz�n.
- �S�?

405
00:35:31,756 --> 00:35:36,272
F�jate. Empezamos a pelear
s�lo por la idea de poder salir.

406
00:35:36,436 --> 00:35:40,111
No creo que hayamos intercambiado
tantas palabras duras antes.

407
00:35:40,276 --> 00:35:45,350
- Las m�as fueron por estar dolorida.
- S�.

408
00:35:45,516 --> 00:35:48,952
Todav�a me encanta que seas extra�o.

409
00:35:49,116 --> 00:35:53,029
Y todav�a me encantan
todas tus excentricidades. . .

410
00:35:53,196 --> 00:35:58,190
Y todav�a te quiero. Como amigo.

411
00:35:58,516 --> 00:36:02,714
�Sobre qu� crees
que iba el sue�o realmente?

412
00:36:02,876 --> 00:36:04,707
No estoy segura.

413
00:36:05,396 --> 00:36:11,665
Quiz�s es que ans�o tanto la intimidad
a cualquier nivel, que. . .

414
00:36:12,636 --> 00:36:18,347
. . .pude haber confundido la intimidad
de una amistad con la clase de. . .

415
00:36:18,516 --> 00:36:20,347
No lo s�.

416
00:36:21,556 --> 00:36:26,391
Lo que s�, la idea de perderte
como amigo,. . .

417
00:36:26,556 --> 00:36:29,787
. . .como mi mejor amigo. . .

418
00:36:30,036 --> 00:36:35,064
- Es algo impensable.
- S�, para m� tambi�n.

419
00:36:35,236 --> 00:36:36,555
As� que. . .

420
00:36:38,556 --> 00:36:40,035
�Una tregua?

421
00:36:41,236 --> 00:36:42,828
Un pacto.

422
00:37:24,356 --> 00:37:29,066
Qu� t�pico. Con los papeles del
divorcio, sentado en la barra.

423
00:37:29,236 --> 00:37:32,911
- �Deber�a estar bailando con Richard?
- Eso ser�a m�s t�pico.

424
00:37:33,876 --> 00:37:38,904
Eres una buena chica, Sandy.
Te pedir�a salir a bailar, pero. . .

425
00:37:39,476 --> 00:37:41,353
Me voy a casa.

426
00:37:41,516 --> 00:37:46,226
- �Necesitas algo de la oficina?
- No, yo me voy tambi�n. Te acompa�o.

427
00:37:46,396 --> 00:37:49,911
- Eso y nada m�s.
- No te ofrezco nada m�s.

428
00:38:07,396 --> 00:38:10,832
Se me ha metido r�mel
en el ojo o algo.

429
00:38:15,916 --> 00:38:20,273
�Te acuestas normalmente en
la primera cita con los hombres?

430
00:38:20,836 --> 00:38:22,155
No.

431
00:38:22,636 --> 00:38:27,266
El hombre se confundi�. lmag�nate
lo que piensa el lavacoches.

432
00:38:27,476 --> 00:38:31,355
Eso fue algo inusual en tu caso, Ally.
Pero, conmigo,. . .

433
00:38:31,556 --> 00:38:36,550
. . .me acueste con muchos o no,
la reputaci�n que tengo. . .

434
00:38:36,716 --> 00:38:39,310
Quiz� sea mi personalidad.

435
00:38:39,476 --> 00:38:43,105
Disfrutas d�ndote a conocer como
alguien sexualmente abierto.

436
00:38:43,796 --> 00:38:48,233
Si la gente cree que eso es ser
una trola, se equivoca.

437
00:38:48,756 --> 00:38:50,235
Eso es todo.

438
00:38:50,556 --> 00:38:54,515
No quiere decir que tengas
que empezar a vestirte as�.

439
00:38:55,036 --> 00:38:59,871
No tienes m�s poder por actuar
como los dem�s quieren.

440
00:39:00,156 --> 00:39:01,987
La verdad es que. . .

441
00:39:02,476 --> 00:39:05,434
-... te admiro.
- S�, claro.

442
00:39:05,596 --> 00:39:09,350
El serm�n que me diste el a�o pasado,
lo de si me gustaba mi vida.

443
00:39:09,516 --> 00:39:13,429
Como no todo el mundo quiere
ser m�dico, abogado o. . .

444
00:39:13,596 --> 00:39:17,475
�Por qu� crees que cant� aquella
canci�n en la fiesta de Navidad?

445
00:39:17,636 --> 00:39:20,867
- Quer�a ser t� por una noche.
- Por favor.

446
00:39:21,076 --> 00:39:26,787
�Cu�nta gente conoces que impregne
la ropa en feromonas?

447
00:39:29,516 --> 00:39:31,950
Eres una optimista.

448
00:39:32,116 --> 00:39:35,267
Eso es lo que quiero ser
yo cuando sea grande.

449
00:39:37,916 --> 00:39:39,907
Estoy sola.

450
00:39:42,956 --> 00:39:44,753
S�, bueno,. . .

451
00:39:45,836 --> 00:39:47,474
. . .y yo.

452
00:39:49,316 --> 00:39:53,355
- Hay cosas peores.
- S�.

453
00:39:54,316 --> 00:39:55,908
�Como qu�?

454
00:39:56,076 --> 00:39:59,625
Tem�a que me preguntases eso.

455
00:39:59,796 --> 00:40:03,630
�Puedo pensarlo en la cena?
�Si pago yo?

456
00:40:03,796 --> 00:40:05,593
Supongo.

457
00:40:08,236 --> 00:40:12,434
Has estado muy pensativo �ltimamente.
�Le est�s dando vueltas a algo?

458
00:40:12,596 --> 00:40:15,429
- No.
- John, ha estado pasando algo.

459
00:40:15,596 --> 00:40:19,669
- Nada de lo que haya que hablar.
- �Nada de que hablar?

460
00:40:19,836 --> 00:40:22,908
- Las parejas no tienen secretos.
- A veces s�.

461
00:40:23,076 --> 00:40:25,988
No s� si me siento c�moda as�.

462
00:40:26,156 --> 00:40:28,909
- �Quieres saber el mayor secreto?
- S�.

463
00:40:32,276 --> 00:40:33,914
Te quiero.

