<?php
/**
 * Subtitle Class
 *
 * PHP Version 5
 * 
 * @category Apps
 * @package  SubtitleModifier
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
namespace Apps\SubtitleModifier;

/**
 * Main
 *
 * @category Apps
 * @package  SubtitleModifier
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
class Main extends \Apps\Base
{
    /**
     * Properties 
     */

    /**
     * El nombre de la aplicacion
     */
    protected $sAppName;

    /**
     * El nombre del archivo a modificar
     */
    private $_sFileName;
    /**
     * El nombre del archivo temporal a modificar
     */
    private $_sFileTmpName;
    /**
     * El contenido del archivo a modificar
     */
    private $_aFileContentArray;
    /**
     * Los segundos a agregar o restar
     */
    private $_iOffset;
    /**
     * La carpeta donde se guardara el subtitulo modificado
     */
    private $_sFolderToSave = '';

    const SUBTITLE_TIME_DELIMITER = ' --> ';
    const MILISECONDS_DELIMITER   = ',';
    const TIME_FORMAT             = 'H:i:s';

    /**
     * Methods 
     */

    /**
     * Método constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->oSession->start();

        $this->sAppName = $this->getAppNameByNamespace(__NAMESPACE__);
    }

    /**
     * Metodo que se encarga de ejecutar la aplicacion.
     * 
     * @return bool
     */
    public function run()
    {
        if ($this->oRequest->oGet->checkGetKey(1) && 
            'subtitles' === $this->oRequest->oGet->getGet(1)
        ) {
            return $this->_processSubtitleDownloadsRequest($this->oRequest->oGet->getGet(1));
        }

        if ($this->oRequest->oPost->requestHasPost()) {
            $this->_processSubmit();
        }

        return $this->_showForm();
    }

    /**
     * Método que muestra el formulario de modificacion del subitulo.
     * Luego de mostrarse las notificaciones de la sesion, la destruye.
     * 
     * @return void
     */
    private function _showForm()
    {
        $aData['oApp'] = $this;

        return \Core\Template\Engine::render(__METHOD__, $aData);
    }

    /**
     * Método que procesa los datos del form submiteado.
     * 
     * @return void
     */
    private function _processSubmit()
    {
        if ($this->_checkValues()) {
            $this->_setValues();
            $this->_correctOffset();
        }

        return true;
    }

    /**
     * Método que valida los datos submiteados.
     * 
     * @return bool
     */
    private function _checkValues()
    {
        $errors = array();

        if (!is_numeric($this->oRequest->oPost->getPost('seconds'))) {
            $errors[] = 'Seconds have to be a number.';
        }

        $this->oRequest->oFiles->setInputFileName('subtitle');

        if ($this->oRequest->oFiles->getUploadedFileError() > 0) {
            $errors[] = 'There was an error while uploading the subtitle.';
        }

        if (!empty($errors)) {
            $_SESSION['messages']['errors'] = $errors;
            return false;
        }

        return true;
    }

    /**
     * Guarda todas las variables en el objeto
     * 
     * @return void
     */
    private function _setValues()
    {
        $this->oRequest->oFiles->setInputFileName('subtitle');

        $this->_sFileName         = $this->oRequest->oFiles->getUploadedFileName();
        $this->_sFileTmpName      = $this->oRequest->oFiles->getUploadedFileTmpName();
        $this->_aFileContentArray = file($this->_sFileTmpName, FILE_IGNORE_NEW_LINES);
        $this->_iOffset           = (int)$this->oRequest->oPost->getPost('seconds');
        $this->_sFolderToSave     = 'subtitles';
    }

    /**
     * Método que contiene la lógica para la modificación del subtitulo.
     * 
     * @return bool
     */
    private function _correctOffset()
    {
        foreach ($this->_aFileContentArray as $index1 => $subtitleItem) {
            //time variable structure: hh:mm:ss,mmm --> hh:mm:ss,mmm
            if (strpos($subtitleItem, self::SUBTITLE_TIME_DELIMITER) !== false) {
                $subtitleItem = explode(self::SUBTITLE_TIME_DELIMITER, $subtitleItem);

                foreach ($subtitleItem as $index2 => $time) {
                    $timeArray = explode(self::MILISECONDS_DELIMITER, $time);

                    $timeArray[0] = date(self::TIME_FORMAT, strtotime($timeArray[0]) + $this->_iOffset);

                    $subtitleItem[$index2] = implode(self::MILISECONDS_DELIMITER, $timeArray);
                }

                $this->_aFileContentArray[$index1] = implode(self::SUBTITLE_TIME_DELIMITER, $subtitleItem);
            }
        }

        try {
            $this->_saveSubtitle();
        } catch (\Exception $e) {
            $this->oSession->setValue('messages.errors', $e->getMessage());

            return false;
        }

        $this->oSession->setValue(
            'messages.success.href',
            \Core\Router\Apps::getUrlByApp($this->sAppName) . DS . 
                $this->_sFolderToSave . DS . 
                $this->oRequest->oFiles->getUploadedFileName()
        );
        $this->oSession->setValue(
            'messages.success.text',
            $this->oRequest->oFiles->getUploadedFileName()
        );

        return true;
    }

    /**
     * Método que se encarga de almacenar los datos del subtítulo en un nuevo archivo
     * 
     * @return bool
     */
    private function _saveSubtitle()
    {
        $sUploadedFileNewPath = PATH_AP_APPS . DS . 
            $this->sAppName . DS . 
            $this->_sFolderToSave . DS . 
            $this->oRequest->oFiles->getUploadedFileName();

        $rNewFile = fopen($sUploadedFileNewPath, 'w');

        if (!is_resource($rNewFile)) {
            throw new \Exception("No se pudo crear el subtitulo modificado.");
        }

        foreach ($this->_aFileContentArray as $value) {
            fwrite($rNewFile, $value . "\r\n");
        }

        fclose($rNewFile);

        return true;
    }

    /**
     * Método que se encarga modificar los headers para que se descargue el archivo solicitado
     * 
     * @param string $sFileName El nombre del archivo a descargar
     * 
     * @return bool
     */
    private function _processSubtitleDownloadsRequest($sFileName)
    {
        $sFilePath = PATH_AP_APPS . DS . $this->sAppName . DS . 'subtitles' . DS . $sFileName;

        header('Content-Type: text/plain');
        header('Content-Disposition: attachment');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($sFilePath));

        ob_clean();
        flush();
        readfile($sFilePath);

        return true;
    }

}
