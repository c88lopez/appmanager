<?php
/**
 * Dado Class
 *
 * PHP Version 5
 * 
 * @category Apps
 * @package  Generala
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
namespace Apps\Generala;

/**
 * Dado
 *
 * @category Apps
 * @package  Generala
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
class Dado
{
    /**
     * Properties 
     */
    /**
     * Representa la cantidad de valores posibles de un dado
     * @var array
     */
    protected $aSides = array(1, 2, 3, 4, 5, 6);

    /**
     * Methods 
     */
    /**
     * Metodo que se vuelve el valor de un dado cualquiera
     * 
     * @return int
     */
    public function dameUnLado()
    {
        shuffle($this->aSides);
        return $this->aSides[0];
    }

}