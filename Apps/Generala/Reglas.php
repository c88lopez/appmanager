<?php
/**
 * Reglas Class
 *
 * PHP Version 5
 *
 * @category Apps
 * @package  Generala
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
namespace Apps\Generala;

/**
 * Reglas
 *
 * @category Apps
 * @package  Generala
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
class Reglas
{
    /**
     * Properties
     */

    /**
     * Methods
     */
    /**
     * Metodo que se encarga de revisar si la combinacion de dados resulta una generala
     *
     * @param array $aRes Lista de valores
     *
     * @return bool
     */
    public function isGenerala($aValues)
    {
        $aUniqueValues = array_unique($aValues);
        if (1 === count($aUniqueValues)) {
            return true;
        }

        return false;
    }

    /**
     * Metodo que se encarga de revisar si la combinacion de dados resulta un poker
     *
     * @param array $aRes Lista de valores
     *
     * @return bool
     */
    public function isPoker($aValues)
    {
        $aUniqueValues = array_count_values($aValues);

        if (1 === count($aUniqueValues)) {
            return true;
        } else if (4 === reset($aUniqueValues) || 4 === next($aUniqueValues)) {
            return true;
        }

        return false;
    }

    /**
     * Metodo que se encarga de revisar si la combinacion de dados resulta un full
     *
     * @param array $aRes Lista de valores
     *
     * @return bool
     */
    public function isFull($aValues)
    {
        $aUniqueValues = array_count_values($aValues);

        $iFirst = reset($aUniqueValues);
        $iLast  = next($aUniqueValues);

        if ((3 === $iFirst && 2 === $iLast) || (2 === $iFirst && 3 === $iLast)) {
            return true;
        }

        return false;
    }

    /**
     * Metodo que se encarga de revisar si la combinacion de dados resulta una escalera
     *
     * @param array $aRes Lista de valores
     *
     * @return bool
     */
    public function isStairs($aValues)
    {
        $aPosibleCombinations = array(
            array(1, 2, 3, 4, 5), // Menor
            array(2, 3, 4, 5, 6), // Mayor
            array(3, 4, 5, 6, 1), // Camino al As
        );

        foreach ($aPosibleCombinations as $aValue) {
            $aDiff = array_diff($aValue, $aValues);
            if (empty($aDiff)) {
                return true;
            }
        }

        return false;
    }

}