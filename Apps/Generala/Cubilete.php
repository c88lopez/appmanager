<?php
/**
 * Cubilete Class
 *
 * PHP Version 5
 * 
 * @category Apps
 * @package  Generala
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
namespace Apps\Generala;

/**
 * Cubilete
 *
 * @category Apps
 * @package  Generala
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
class Cubilete
{
    /**
     * Properties 
     */
    /**
     * Representa la coleccion de dados que posee el cubilete
     * @var array
     */
    protected $aDados = array();

    /**
     * Methods 
     */
    /**
     * Metodo que mezcla los dados y retorna el listado dew valores
     *
     * @param int $iCantDados La cantidad de dados a mezclar
     *
     * @return array
     */
    public function mezclar($iCantDados = 0)
    {
        $aResult = array();
        $this->setDados($iCantDados);

        for ($i=0; $i < $iCantDados; $i++) {
            $aResult[] = $this->aDados[$i]->dameUnLado();
        }

        return $aResult;
    }

    /**
     * Metodo que carga la cantidad de dados indicadas en el cubilete
     * 
     * @param int $iCant Cantidad de dados a cargar
     */
    protected function setDados($iCant = 0)
    {
        for ($i=0; $i < $iCant; $i++) { 
            $this->aDados[$i] = new \Apps\Generala\Dado;
        }
    }

}