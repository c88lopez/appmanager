<?php
/**
 * Generala Class
 *
 * PHP Version 5
 * 
 * @category Apps
 * @package  Generala
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
namespace Apps\Generala;

/**
 * Main
 *
 * @category Apps
 * @package  Generala
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
class Main extends \Apps\Base
{
    /**
     * Properties 
     */

    /**
     * Methods 
     */
    /**
     * Metodo que ejecuta la aplicacion y gestiona sus peticiones
     * 
     * @return bool
     */
    public function run()
    {


        $this->showGame();
    }

    /**
     * Metodo que muestra el juego
     *
     * @return bool
     */
    protected function showGame()
    {
        return \Core\Template\Engine::render(__METHOD__);
    }



}
