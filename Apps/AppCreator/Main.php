<?php
/**
 * AppCreator Class
 *
 * PHP Version 5
 * 
 * @category Apps
 * @package  AppCreator
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
namespace Apps\AppCreator;

/**
 * Main
 *
 * @category Apps
 * @package  AppCreator
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
class Main extends \Apps\Base
{
    /**
     * Properties 
     */
    /**
     * Representa el nombre de la nueva aplicacion
     */
    protected $_sAppName;
    /**
     * Representa la lista de errores
     */
    private $_aErrors = array();
    /**
     * Representa la descripcion de la nueva aplicacion
     */
    private $_sAppDescription;


    /**
     * Methods 
     */

    /**
     * Método constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_sAppName = $this->getAppNameByNamespace(__NAMESPACE__);
    }

    /**
     * Metodo que se encarga de ejecutar la aplicacion.
     *
     * @return bool
     */
    public function run()
    {
        if ($this->oRequest->oPost->requestHasPost() && $this->_validateForm()) {
            $this->_createApp();
        } else {
            return $this->_showForm();
        }

        return \Core\Router\Url::redirectInit();
    }

    /**
     * Método que muestra el formulario de modificacion del subitulo.
     * Luego de mostrarse las notificaciones de la sesion, la destruye.
     * 
     * @return bool
     */
    private function _showForm()
    {
        $aData['sAppName'] = $this->_sAppName;
        $aData['aErrors']  = $this->_aErrors;

        return \Core\Template\Engine::render(__METHOD__, $aData);
    }

    /**
     * Método que valida los datos ingresados
     * 
     * @return bool
     */
    private function _validateForm()
    {
        $bIsValid = true;

        $this->_sAppName        = $this->parseAppName();
        $this->_sAppDescription = $this->oRequest->oPost->getPost('appDescription');

        if (empty($this->_sAppName)) {
            $this->_aErrors[] = 'El nombre para la nueva aplicacion no puede ser vacio';
            $bIsValid = false;
        }

        if (\Core\Validation\Apps::appExists($this->_sAppName)) {
            $this->_aErrors[] = 'El nombre para la nueva aplicacion ya existe';
            $bIsValid = false;
        }

        return $bIsValid;
    }

    /**
     * Metodo que contiene el proceso para la cracion de una nueva aplicacion
     */
    private function _createApp()
    {
        $this->_createFolders();
        $this->_createFiles();
    }

    /**
     * Metodo que crea la carpeta de la aplicacion en Apps y en Templates/Apps
     *
     * @return bool
     */
    private function _createFolders()
    {
        mkdir(PATH_AP_APPS . DS . ucfirst($this->_sAppName), 0775);
        mkdir(PATH_AP_TEMPLATES . DS . 'Apps' . DS . ucfirst($this->_sAppName) . DS . 'Main', 0775, true);

        return true;
    }

    /**
     * Metodo que crea la carpeta de la aplicacion en Apps y en Templates/Apps
     *
     * @return bool
     */
    private function _createFiles()
    {
        touch(PATH_AP_APPS . DS . ucfirst($this->_sAppName) . DS . 'Main.php');
        $sNewAppTpl = file_get_contents(PATH_AP_TEMPLATES . DS . 'Common' . DS .'NewApp.php');
        $sNewAppTplParsed = preg_replace('/\-AppName\-/', ucfirst($this->_sAppName), $sNewAppTpl);

        file_put_contents(PATH_AP_APPS . DS . ucfirst($this->_sAppName) . DS . 'Main.php', $sNewAppTplParsed);

        touch(PATH_AP_APPS . DS . ucfirst($this->_sAppName) . DS . 'AppInfo.json');
        $aAppInfo = array(
            'title'       => trim($this->oRequest->oPost->getPost('appName')),
            'description' => $this->_sAppDescription,
            'active'      => true,
        );
        $sJsonContent = json_encode($aAppInfo);

        file_put_contents(PATH_AP_APPS . DS . ucfirst($this->_sAppName) . DS . 'AppInfo.json', $sJsonContent);
        return true;
    }

    /**
     * Metodo que se encarga de retornar el nombre de correcto segun el ingresado
     */
    private function parseAppName()
    {
        return implode(
            '', 
            array_map(
                'ucfirst',
                explode(
                    ' ', 
                    trim(
                        $this->oRequest->oPost->getPost('appName')
                    )
                )
            )
        );
    }

}
