<?php
/**
 * Gastos Class
 *
 * PHP Version 5
 * 
 * @category Apps
 * @package  Gastos
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
namespace Apps\Gastos;

/**
 * Main
 *
 * @category Apps
 * @package  Gastos
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
class Main extends \Apps\Base
{
    /**
     * Properties
     */
    /**
     * El nombre de la aplicacion
     */
    protected $sAppName;

    /**
     * Methods
     */
    /**
     * Método constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->oSession->start();

        $this->sAppName = $this->getAppNameByNamespace(__NAMESPACE__);
    }

    /**
     * Metodo que ejecuta la aplicacion y gestiona sus peticiones
     * 
     * @return bool
     */
    public function run()
    {
        /**
         * Seccion por defecto
         */
        $sSeccion = 'Lista';
        $aArguments = $this->oRequest->oGet->getGet();
        if (isset($aArguments[1])) {
            $sSeccion = $aArguments[1];
        }

        /**
         * Lista de secciones disponibles
         */
        $aRouteSection = array(
            'Lista'      => 'showList',
            'Formulario' => 'showForm',
            'Eliminar'   => 'delete',
            'Submit'     => 'submit',
            'Grafico'    => 'showGraph',
        );

        return $this->$aRouteSection[$sSeccion]();
    }

    /**
     * Metodo que se ejecuta por defecto o por demanda
     * 
     * @return bool
     */
    public function showList()
    {
        $aData['sAppName'] = $this->getAppName();
        $aData['aGastos']  = $this->oDB->callFunction('Gastos_list', array(), true);

        return \Core\Template\Engine::render(__METHOD__, $aData);
    }

    /**
     * Metodo que se ejecuta por demanda
     * 
     * @return bool
     */
    public function showForm()
    {
        /**
         * Se obtiene los datos por GET
         */
        $aData['sAppName'] = $this->getAppName();
        if ($this->oRequest->oGet->checkGetKey(2)) {
            $aData['iGastoID'] = $this->oRequest->oGet->getGet(2);
        }

        /**
         * Reviso si se guardaron los datos del formulario en sesion debido a un error en la validacion
         */
        $aFormData = $this->oSession->getFormData();
        if (empty($aFormData) && isset($aData['iGastoID'])) {
            $aGastoData = $this->oDB->callFunction('Gastos_select', $aData['iGastoID'], true);

            /**
             * Al no encontrarse datos del formulario en la sesion se obtienen los valores de la base de 
             * datos por seleccionar una edicion
             */
            if (!empty($aGastoData)) {
                $aFormData['gastoID'] = $aGastoData[0]['id'];
                $aFormData['fecha']   = $aGastoData[0]['fecha'];
                $aFormData['detalle'] = $aGastoData[0]['detalle'];
                $aFormData['monto']   = $aGastoData[0]['monto'];
                $aFormData['autor']   = $aGastoData[0]['autor'];
                $aFormData['grupoID'] = $aGastoData[0]['id_grupo'];
            }
        }

        /**
         * Obtengo y cargo los grupos para los gastos
         */
        $aGastosGruposData = $this->oDB->callFunction('Gastos_grupos_select_all', array(), true);
        $aData['gastosGrupos'] = $aGastosGruposData;

        /**
         * Se cargan los datos a mostrar en el formulario
         */
        $aData['formData'] = array(
            'gastoID' => isset($aFormData['gastoID']) ? $aFormData['gastoID'] : '',
            'fecha'   => isset($aFormData['fecha'])   ? $aFormData['fecha']   : '',
            'detalle' => isset($aFormData['detalle']) ? $aFormData['detalle'] : '',
            'monto'   => isset($aFormData['monto'])   ? $aFormData['monto']   : '',
            'autor'   => isset($aFormData['autor'])   ? $aFormData['autor']   : '',
            'grupoID' => isset($aFormData['grupoID']) ? $aFormData['grupoID'] : '',
        );

        return \Core\Template\Engine::render(__METHOD__, $aData);
    }

    /**
     * Metodo que se ejecuta por demanda
     * 
     * @return bool
     */
    public function showGraph()
    {


        return \Core\Template\Engine::render(__METHOD__);
    }

    /**
     * Metodo que agrega o edita un gasto
     * 
     * @return bool
     */
    public function submit()
    {
        $aFormValues = $this->oRequest->oPost->getPost();

        if ($this->validateSubmit($aFormValues)) {
            $sSqlFunction = 'Gastos_insert';
            $sMessage     = 'Gasto agregado con exito';
            if (is_numeric($aFormValues['gastoID'])) {
                $sSqlFunction = 'Gastos_update';
                $aArguments[] = $aFormValues['gastoID'];
                $sMessage     = 'Gasto modificado con exito';
            }

            $aArguments[] = $aFormValues['fecha'];
            $aArguments[] = $aFormValues['detalle'];
            $aArguments[] = $aFormValues['monto'];
            $aArguments[] = $aFormValues['autor'];
            $aArguments[] = $aFormValues['grupoID'];

            $this->oDB->callFunction($sSqlFunction, $aArguments);

            $this->oSession->addMessage(
                $sMessage, 
                \Core\Storage\Session::MSG_LEVEL_SUCCESS
            );
            \Core\Router\Apps::redirectUrlByApp($this->getAppName());
        } else {
            $this->oSession->addFormData($aFormValues);
            \Core\Router\Apps::redirectUrlByApp($this->getAppName(), array('Formulario'));
        }

        return true;
    }

    /**
     * Metodo que se encarga de validar los datos del formulario
     *
     * @param array $aFormValues array asociativo donde la key es el nombre del campo y su valor el ingresado.
     *
     *
     * @return bool
     */
    protected function validateSubmit($aFormValues)
    {
        $aValidationRules = array(
            'gastoID' => array(
                'type' => 'numeric', 
            ),
            'fecha' => array(
                'mandatory', 
                'type'   => 'date', 
                'format' => 'dd/mm/yyyy'
            ),
            'detalle' => array(
                'mandatory', 
                'type' => 'alphanumeric', 
            ),
            'monto' => array(
                'mandatory', 
                'type'   => 'float',
                'format' => 'dd',
            ),
            'autor' => array(
                'mandatory', 
                'type'        => 'alphanumeric', 
                'validFromDB' => array(
                    'table' => 'Gastos_gastos_autores',
                    'field' => 'autor',
                ),
            ),
            'grupoID' => array(
                'mandatory', 
                'type' => 'numeric', 
                'validFromDB' => array(
                    'table' => 'Gastos_gastos_grupos',
                    'field' => 'id',
                ),
            ),
        );

        $bValid = true;
        $aErrors = \Core\Validation\Input::validate(
            $aFormValues, 
            $aValidationRules
        );

        if (!empty($aErrors)) {
            foreach ($aErrors as $sError) {
                $this->oSession->addMessage(
                    $sError, 
                    \Core\Storage\Session::MSG_LEVEL_ERROR
                );
            }

            $bValid = false;
        }

        return $bValid;
    }

    /**
     * Metodo que elimina un gasto
     * 
     * @return bool
     */
    public function delete()
    {
        $aArguments = array(
            $this->oRequest->oGet->getGet(2),
        );
        $this->oDB->callFunction('Gastos_delete', $aArguments);

        $this->oSession->addMessage('Gasto eliminado con exito', \Core\Storage\Session::MSG_LEVEL_SUCCESS);
        \Core\Router\Apps::redirectUrlByApp($this->getAppName());

        return true;
    }

}
