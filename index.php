<?php

/**
 * index.php
 * Archivo index
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Init
 * @subpackage IndexFile
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
$fStartTime = microtime(true);
require_once 'Core' . DIRECTORY_SEPARATOR . 'Config' . DIRECTORY_SEPARATOR . 'bootstrap.php';
var_dump(microtime(true) - $fStartTime);