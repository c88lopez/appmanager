<?php

/**
 * CubileteTest.php
 * Archivo que contiene el test de la clase Cubilete
 *
 * PHP Version 5
 *
 * @category   AppManager
 * @package    Tests
 * @subpackage Apps
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Tests\Apps\Generala;

/**
 * CubileteTest
 * Clase que se encarga de validar la funcionalidad de dicha clase
 *
 * @category   AppManager
 * @package    Tests
 * @subpackage Apps
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class CubileteTest extends \PHPUnit_Framework_TestCase
{
    protected $oCubilete;

    protected function setUp()
    {
        $this->oCubilete = new \Apps\Generala\Cubilete;
    }

    protected function tearDown()
    {
        unset($this->oCubilete);
    }

    public function testAsd()
    {
        $aDados = $this->oCubilete->mezclar(3);

        $this->assertTrue(3 === count($aDados));
    }
}
