<?php

/**
 * DadoTest.php
 * Archivo que contiene el test de la clase Dado
 *
 * PHP Version 5
 *
 * @category   AppManager
 * @package    Tests
 * @subpackage Apps
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Tests\Apps\Generala;

/**
 * DadoTest
 * Clase que se encarga de validar la funcionalidad de dicha clase
 *
 * @category   AppManager
 * @package    Tests
 * @subpackage Apps
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class DadoTest extends \PHPUnit_Framework_TestCase
{
    protected $oDado;

    protected function setUp()
    {
        $this->oDado = new \Apps\Generala\Dado;
    }

    protected function tearDown()
    {
        unset($this->oDado);
    }

    public function testValorNumerico()
    {
        $iValor = $this->oDado->dameUnLado();
        $this->assertTrue(is_numeric($iValor));
    }
}
