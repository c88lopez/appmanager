<?php

/**
 * ReglasTest.php
 * Archivo que contiene el test de la clase Reglas
 *
 * PHP Version 5
 *
 * @category   AppManager
 * @package    Tests
 * @subpackage Apps
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Tests\Apps\Generala;

/**
 * ReglasTest
 * Clase que se encarga de validar la funcionalidad de dicha clase
 *
 * @category   AppManager
 * @package    Tests
 * @subpackage Apps
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class ReglasTest extends \PHPUnit_Framework_TestCase
{
    protected $oReglas;

    protected function setUp()
    {
        $this->oReglas = new \Apps\Generala\Reglas;
    }

    protected function tearDown()
    {
        unset($this->oReglas);
    }

    public function testGenerala()
    {
        $aDados = array(1, 1, 1, 1, 1);
        $this->assertTrue($this->oReglas->isGenerala($aDados));

        $aDados = array(1, 1, 1, 1, 2);
        $this->assertTrue(!$this->oReglas->isGenerala($aDados));

        $aDados = array(2, 1, 1, 1, 1);
        $this->assertTrue(!$this->oReglas->isGenerala($aDados));
    }

    public function testPoker()
    {

        $aDados = array(1, 1, 1, 1, 1);
        $this->assertTrue($this->oReglas->isPoker($aDados));

        $aDados = array(1, 1, 1, 1, 2);
        $this->assertTrue($this->oReglas->isPoker($aDados));

        $aDados = array(1, 1, 1, 2, 2);
        $this->assertTrue(!$this->oReglas->isPoker($aDados));

        $aDados = array(2, 1, 1, 2, 2);
        $this->assertTrue(!$this->oReglas->isPoker($aDados));
    }

    public function testFull()
    {
        $aDados = array(1, 1, 1, 2, 2);
        $this->assertTrue($this->oReglas->isFull($aDados));

        $aDados = array(2, 1, 1, 2, 2);
        $this->assertTrue($this->oReglas->isFull($aDados));

        $aDados = array(2, 2, 1, 2, 2);
        $this->assertTrue(!$this->oReglas->isFull($aDados));

        $aDados = array(1, 1, 1, 1, 1);
        $this->assertTrue(!$this->oReglas->isFull($aDados));

        $aDados = array(1, 2, 3, 4, 5);
        $this->assertTrue(!$this->oReglas->isFull($aDados));
    }

    public function testStairs()
    {
        $aDados = array(5, 2, 3, 4, 1);
        $this->assertTrue($this->oReglas->isStairs($aDados));

        $aDados = array(3, 5, 6, 1, 4);
        $this->assertTrue($this->oReglas->isStairs($aDados));

        $aDados = array(2, 4, 6, 3, 5);
        $this->assertTrue($this->oReglas->isStairs($aDados));

        $aDados = array(1, 1, 1, 2, 2);
        $this->assertTrue(!$this->oReglas->isStairs($aDados));

        $aDados = array(2, 1, 1, 2, 2);
        $this->assertTrue(!$this->oReglas->isStairs($aDados));

        $aDados = array(2, 2, 1, 2, 2);
        $this->assertTrue(!$this->oReglas->isStairs($aDados));

        $aDados = array(1, 1, 1, 1, 1);
        $this->assertTrue(!$this->oReglas->isStairs($aDados));
    }
}
