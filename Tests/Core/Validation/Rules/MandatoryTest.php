<?php

/**
 * MandatoryTest.php
 * Archivo que contiene el test de la clase MandatoryTest
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Tests
 * @subpackage Validation
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Tests\Core\Validation\Rules;

/**
 * MandatoryTest
 * Clase que se encarga de validar la funcionalidad de dicha clase
 *
 * @category   AppManager
 * @package    Tests
 * @subpackage Validation
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class MandatoryTest extends \PHPUnit_Framework_TestCase
{
    public function testNotEmptyValue()
    {
        $oMandatoryRule = new \Core\Validation\Rules\Mandatory();
        $oMandatoryRule->setInputData('fieldName', 'fieldValue');

        $this->assertTrue($oMandatoryRule->validate());
    }

    /**
     * @expectedException Exception
     */
    public function testEmptyValueException()
    {
        $oMandatoryRule = new \Core\Validation\Rules\Mandatory();
        $oMandatoryRule->setInputData('fieldName', '');

        $oMandatoryRule->validate();
    }

    /**
     * @expectedException Exception
     */
    public function testUnsetValueException()
    {
        $oMandatoryRule = new \Core\Validation\Rules\Mandatory();

        $oMandatoryRule->validate();
    }

}
