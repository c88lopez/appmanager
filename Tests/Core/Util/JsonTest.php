<?php

/**
 * JsonTest.php
 * Archivo que contiene el test de la clase JsonTest
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Tests
 * @subpackage Util
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Tests\Core\Util;

/**
 * JsonTest
 * Clase que se encarga de validar la funcionalidad de dicha clase
 *
 * @category   AppManager
 * @package    Tests
 * @subpackage Util
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class JsonTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException Exception
     */
    public function testEmptyPath()
    {
        \Core\Util\Json::decodeJsonFile('');
    }

    /**
     * @expectedException Exception
     */
    public function testWrongPath()
    {
        \Core\Util\Json::decodeJsonFile('asd');
    }

    public function testSuccessfulDecode()
    {
        \Core\Util\Json::decodeJsonFile('/var/www/sandbox/AppManager/Tests/Core/Util/JsonTest.json');
    }
}
