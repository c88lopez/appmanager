<?php

/**
 * DBTest.php
 * Archivo que contiene el test de la clase DBTest
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Tests
 * @subpackage Config
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Tests\Core\Config;

/**
 * DBTest
 * Clase que se encarga de validar la funcionalidad de dicha clase
 *
 * @category   AppManager
 * @package    Tests
 * @subpackage Config
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class DBTest extends \PHPUnit_Framework_TestCase
{
    public function testExistingParameters()
    {
        $aConnectionParameters = array(
            'host',
            'dbname',
            'user',
            'password',
            'driver',
        );
        $aConnectionData = \Core\Config\Storage\DB::$aConnection;

        foreach ($aConnectionParameters as $sParameter) {
            $this->assertTrue(
                isset($aConnectionData[$sParameter]) && 
                '' !== $aConnectionData[$sParameter]
            );
        }
    }

}
