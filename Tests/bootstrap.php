<?php

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

$sAMPath = dirname(dirname(__FILE__));

require_once $sAMPath . DS . 'Core' . DS . 'Config' . DS . 'autoload.php';
$oSCL = new SplClassLoader(null, $sAMPath);
$oSCL->register();

?>