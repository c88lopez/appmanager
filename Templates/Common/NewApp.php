<?php
/**
 * -AppName- Class
 *
 * PHP Version 5
 * 
 * @category Apps
 * @package  -AppName-
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
namespace Apps\-AppName-;

/**
 * Main
 *
 * @category Apps
 * @package  -AppName-
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  2013 nolicense
 * @link     nolink
 */
class Main extends \Apps\Base
{
    /**
     * Properties 
     */


    /**
     * Methods 
     */
    
    /**
     * Metodo que ejecuta la aplicacion y gestiona sus peticiones
     * 
     * @return bool
     */
    public function run()
    {
        $this->exampleForm();
    }
    
    /**
     * Metodo que muestra el index de la aplicacion
     * 
     * @return bool
     */
    public function exampleForm()
    {
        \Core\Template\Engine::render(__METHOD__);
    }

}
