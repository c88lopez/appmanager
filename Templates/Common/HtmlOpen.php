<?php

/**
 * IndexApps.php
 * Archivo que contiene la clase IndexApps
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Apps
 * @subpackage IndexApps
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */

?>
<html>
    <head>
        <title><?php echo \Core\Template\Engine::getTitleHeadText(); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Cargo CSS -->
        <?php echo \Core\Template\Engine::getHtmlTags('css'); ?>
        
        <!-- Cargo JS -->
        <?php echo \Core\Template\Engine::getHtmlTags('js'); ?>
    </head>
    <body>