<?php

/**
 * IndexApps.php
 * Archivo que contiene la clase IndexApps
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Apps
 * @subpackage IndexApps
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */

?>

<nav class="navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo \Core\Router\Url::getInitUrl(); ?>"><?php 
            echo \Core\Config\Template\Engine::getHeadTagTitle(); 
        ?></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Actions&nbsp;<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Manage Application</a></li>
                </ul>
            </li>
        </ul>
        <form class="navbar-form navbar-right" role="search" method="post">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search App" onkeyup="javascript: filterApps(this); return false;">
            </div>
        </form>
    </div>
    <!-- /.navbar-collapse -->
</nav>
<ol class="breadcrumb">
    <li class="active"><a href="<?php echo \Core\Router\Url::getInitUrl(); ?>"><?php 
        echo \Core\Config\Template\Engine::getHeadTagTitle(); 
    ?></a></li>
    <?php 
        $aUrlArguments = \Core\Router\Url::getUrlArguments();
        if(!empty($aUrlArguments[0])) { ?>
        
    <li class="active"><a href="<?php echo \Core\Router\Apps::getUrlByApp($aUrlArguments[0]); ?>"><?php 
        echo $aUrlArguments[0];
    ?></a></li>
    <?php
        }
    ?>
</ol>
<?php
    $aMessages = \Core\Storage\Session::getMessages();
    if (null !== $aMessages) {
        foreach ($aMessages as $sLevel => $aMessagesLevel) {
            $iMessageLevelCount = count($aMessagesLevel);
?>
                <div class="alert alert-dismissable alert-<?php echo $sLevel; ?>">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <ul>
                        <?php
                            for ($i=0; $i < $iMessageLevelCount; $i++) { 
                        ?>
                                <li><?php echo $aMessagesLevel[$i]; ?></li>
                        <?php
                            }
                        ?>
                    </ul>
                </div>
<?php
        }
    }
?>

<div class="container" style="min-height: 500px;">