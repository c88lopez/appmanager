<?php

/**
 * showAppsTpl.php
 * Archivo Template
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    IndexApps
 * @subpackage 
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */

?>

<div class="row" id="appsContainer">
    <?php 
        $aAppList  = \Apps\IndexApps::getAppsList();
        $iCantApps = count($aAppList);

        /**
         * Representa la cantidad de columnas que se utilizara para el contenedor de la aplicacion.
         * Este sera calculado segun la cantidad de aplicaciones
         * @var integer
         */
        $iGridNumber = 3;
        if ($iCantApps <= 4 && $iCantApps > 0) {
            $iGridNumber = (12 / $iCantApps);
        }

        for ($i=0 ; $i < $iCantApps ; $i++) { ?>

    <div class="col-md-<?php echo $iGridNumber; ?>" 
        style="border-color: black; border-radius: 1px; text-align: center;" id="<?php echo $aAppList[$i]['title']; ?>">
        <h1><?php
        echo $aAppList[$i]['title'];

    ?></h1>
        <p><?php echo $aAppList[$i]['description']; ?></p>
        <p><a class="btn btn-primary" href="<?php echo \Core\Router\Apps::getUrlByApp($aAppList[$i]['path']); ?>">Ejecutar</a></p>
    </div>
    <?php
        }
    ?>

</div>
<script type="text/javascript">
    function filterApps(inputElement)
    {
        /**
         * Obtenemos la coleccion de DOM de cada celda que contiene la aplicacion
         */
        aAppList = document.getElementById('appsContainer').children;
        iAppCount = aAppList.length;

        /**
         * Procesamos el filtro para ocultar aquellas aplicaciones que no cumplen con el texto ingresado
         */
        iMatchedAppsCount = 0;
        for (i=0 ; i < iAppCount ; i++) {
            if (0 !== aAppList[i].id.toLowerCase().indexOf(inputElement.value.toLowerCase())) {
                aAppList[i].style.display = 'none';
            } else {
                aAppList[i].style.display = '';
                iMatchedAppsCount++;
            }
        }

        /**
         * Se realiza el calculo para el tamaño del grid de acuerdo con las aplicaciones que cumplen con el filtro
         */
        iGridNumber = 3;
        if (iMatchedAppsCount <= 4) {
            iGridNumber = (12 / iMatchedAppsCount);
        }

        /**
         * Se cambia el nombre de la clase de acuerdo al valor anterior
         */
        for (i=0 ; i < iAppCount ; i++) {
            sNewClassName = 'col-md-' + iGridNumber;
            aAppList[i].className = sNewClassName;
        }
    }
</script>