<?php
/**
 * Juego Template
 *
 * PHP Version 5
 *
 * @category   AppManager
 * @package    Templates
 * @subpackage Generala
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    2013 nolicense
 * @link       nolink
 */

?>

<div class="row">
    <div class="col-md-8"></div>
    <div class="col-md-4">

        <table class="table table-bordered table-condensed">
            <tr>
                <th>&nbsp;</th>
                <th><center>Jugador 1</center></th>
                <th><center>Jugador 2</center></th>
                <th><center>Jugador 3</center></th>
                <th><center>Jugador 4</center></th>
                <th><center>Jugador 5</center></th>
            </tr>
            <tr>
                <th><center>1</center></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th><center>2</center></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th><center>3</center></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th><center>4</center></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th><center>5</center></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th><center>6</center></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th><center>Escalera</center></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th><center>Full</center></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th><center>Poker</center></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th><center>Generala</center></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th><center>Generala</br>Doble</center></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </table>
    </div>
</div>

<script type="text/javascript">

</script>
