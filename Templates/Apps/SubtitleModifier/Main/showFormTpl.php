<?php
/**
 * Subtitle Form Template
 *
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Templates
 * @subpackage SubtitleModifier
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    2013 nolicense
 * @link       nolink
 */

?>

        <form class="form-horizontal" enctype="multipart/form-data" method="post" 
            action="<?php echo \Core\Router\Apps::getUrlByApp($oApp->getAppName()); ?>">
            <?php
                if ($oApp->oSession->checkKey('messages.success')) { ?>
                    <div class="alert alert-success">
                        <?php echo $oApp->oSession->getValue('messages.success.text'); ?>
                    </div>
            <?php
                } else if ($oApp->oSession->checkKey('messages.errors')) { ?>
                    <div class="alert alert-error">
                        <ul style="margin-bottom: 0px;">
                            <?php echo "<li>" . implode("</li><li>", $oApp->oSession->getValue('messages.errors')) . "</li>" ?>
                        </ul>
                    </div>
            <?php
                }
            ?>
            <div class="form-group">
                <label class="col-lg-2 control-label" for="inputFile">Subtitle</label>
                <div class="col-lg-4">
                    <input type="file" name="subtitle" class="form-control" id="inputFile">
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="inputSeconds">Seconds</label>
                <div class="col-lg-4">
                    <input type="text" name="seconds" class="form-control" id="inputSeconds">
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary" id="submit" disabled="disabled" name="submit">Update</button>
                </div>
            </div>
        </form>

        <script type="text/javascript">
            function checkCompletedForm()
            {
                if( document.getElementById( 'inputFile' ).value != '' && document.getElementById( 'inputSeconds' ).value != '' ) {
                    document.getElementById( 'submit' ).disabled = null;
                } else {
                    document.getElementById( 'submit' ).disabled = "disabled";
                }
            }
            window.setInterval( function() { checkCompletedForm() }, 50 );
        </script>
