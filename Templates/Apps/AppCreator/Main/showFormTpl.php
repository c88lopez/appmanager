<?php
/**
 * Subtitle Form Template
 *
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Templates
 * @subpackage SubtitleModifier
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    2013 nolicense
 * @link       nolink
 */

?>

        <form class="form-horizontal" enctype="multipart/form-data" method="post" 
            action="<?php echo \Core\Router\Apps::getUrlByApp($sAppName); ?>">

            <div class="form-group">
                <label class="col-lg-2 control-label" for="inputFile">Application Name</label>
                <div class="col-lg-4">
                    <input type="text" name="appName" class="form-control" id="inputAppName">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label" for="inputFile">Application Description</label>
                <div class="col-lg-4">
                    <input type="text" name="appDescription" class="form-control" id="inputAppDescription">
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary" id="submit" disabled="disabled" name="submit">Create</button>
                </div>
            </div>

        </form>

        <script type="text/javascript">
            function checkCompletedForm()
            {
                if (document.getElementById( 'inputAppName' ).value != '' && document.getElementById( 'inputAppDescription' ).value != '') {
                    document.getElementById( 'submit' ).disabled = null;
                } else {
                    document.getElementById( 'submit' ).disabled = "disabled";
                }
            }
            window.setInterval( function() { checkCompletedForm() }, 50 );
        </script>
