<?php
/**
 * Subtitle Form Template
 *
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Templates
 * @subpackage Gastos
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    2013 nolicense
 * @link       nolink
 */

use \Core\Router\Apps;
?>

        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#">Lista</a></li>
                    <!--<li><a href="<?php echo Apps::getUrlByApp($sAppName, array('Grafico')); ?>">Gr&aacute;fico</a></li>-->
                    <li><a href="<?php echo Apps::getUrlByApp($sAppName, array('Formulario')); ?>">Ingresar/Editar</a></li>
                </ul>
            </div>
        </div>

        <div class="row" style="height: 25px;"></div>

        <div class="row">
            <div class="col-md-12">
                <?php if ($iGastosCount = count($aGastos)) { ?>
                    <table id="lista_gastos" class="table table-condensed table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Grupo</th>
                                <th>Detalle</th>
                                <th>Monto</th>
                                <th>Autor</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $iGastosCount = count($aGastos);
                                for ($i=0; $i < $iGastosCount; $i++) { 
                            ?>
                            <tr>
                                <td style="width: 100px;">
                                    <?php echo date('d/m/Y', strtotime($aGastos[$i]['fecha']));?>
                                </td>
                                <td style="width: 140px;"><?php echo $aGastos[$i]['grupo'];?></td>
                                <td><?php echo $aGastos[$i]['detalle'];?></td>
                                <td style="width: 95px;">$ <?php echo $aGastos[$i]['monto'];?></td>
                                <td style="width: 80px;"><?php echo $aGastos[$i]['autor'];?></td>
                                <td style="width: 130px;">
                                    <a href="<?php echo Apps::getUrlByApp($sAppName, array('Formulario', $aGastos[$i]['id'])); ?>">Editar</a> | 
                                    <a onclick="javascript: return eliminarGasto();" 
                                        href="<?php echo Apps::getUrlByApp($sAppName, array('Eliminar', $aGastos[$i]['id'])); ?>">Eliminar</a>
                                </td>
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                <?php } else { ?>
                    <p>No hay gastos registrados</p>
                <?php } ?>
            </div>
        </div>

        <script type="text/javascript">
            function eliminarGasto()
            {
                return confirm("Esta seguro que desea eliminar este gasto?")
            }
        </script>
