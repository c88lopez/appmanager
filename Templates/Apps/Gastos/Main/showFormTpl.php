<?php
/**
 * Gastos Lista Template
 *
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Templates
 * @subpackage Gastos
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    2013 nolicense
 * @link       nolink
 */

use \Core\Router\Apps;
?>
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <li><a href="<?php echo Apps::getUrlByApp($sAppName); ?>">Lista</a></li>
                    <!--<li><a href="#">Gr&aacute;fico</a></li>-->
                    <li class="active">
                        <a href="<?php echo Apps::getUrlByApp($sAppName, array('Formulario')); ?>">Ingresar/Editar</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row" style="height: 25px;"></div>

        <div class="row">
            <div class="col-md-offset-2 col-md-12">
                <form name="form" id="form" class="form-horizontal" 
                    action="<?php echo Apps::getUrlByApp($sAppName, array('Submit')); ?>" method="post" role="form">
                    <input type="hidden" name="gastoID" id="gastoID" 
                        value="<?php echo isset($iGastoID)?$iGastoID:''; ?>" />

                    <div class="form-group">
                        <div class="input-group col-md-3">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            <input type ="text" name="fecha" id="fecha" class="form-control" placeholder="Fecha" 
                                autocomplete="off" value="<?php echo $formData['fecha']; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-md-3">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></span>
                            <input type ="text" name="detalle" id="detalle" class="form-control" placeholder="Detalle" 
                                value="<?php echo $formData['detalle']; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-md-3">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-usd"></span></span>
                            <input type ="text" name="monto" id="monto" class="form-control" placeholder="Monto" 
                                value="<?php echo $formData['monto']; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-md-3">
                            <select name="autor" id="autor" class="form-control" autocomplete="off">
                                <option value="">Seleccionar autor...</option>
                                <option <?php echo ('Cristian' === $formData['autor'])?'selected':''; ?> value="Cristian">Cristian</option>
                                <option <?php echo ('Soledad' === $formData['autor'])?'selected':''; ?> value="Soledad">Soledad</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-md-3">
                            <select name="grupoID" id="grupoID" class="form-control" autocomplete="off">
                                <option value="">Seleccionar grupo...</option>
                                <?php foreach ($gastosGrupos as $aGastoData) { ?>
                                    <option <?php echo ($aGastoData['id'] === $formData['grupoID'])?'selected':''; ?> 
                                        value="<?php echo $aGastoData['id']; ?>">
                                        <?php echo $aGastoData['grupo']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary">
                                <?php echo (isset($iGastoID)) ? 'Modificar' : 'Ingresar'; ?>
                            </button>
                            <button type="reset" id="reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                $("#fecha").datepicker({
                    dateFormat: "yy-dd-mm",
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    maxDate: 0
                });

                $("#reset").click(function() {
                    $("#fecha").val('');
                    $("#detalle").val('');
                    $("#monto").val('');
                    $("#autor option:selected").removeAttr("selected");
                    $("#grupo option:selected").removeAttr("selected");
                });

                /**
                 * Porcion de logica que se encarga de que solo se puedan ingresar numeros y/o un punto
                 */
                $("#monto").keypress(function(e) {
                    bMatch = true;

                    /**
                     * Comprueba que solo se ingresen numeros, puntos o comas
                     */
                    if ((e.which >= 48 && e.which <= 57) || e.which == 46)
                    {
                        bMatch = false;
                    }

                    /**
                     * Comprueba que solo se ingrese un punto
                     */
                    if (e.which == 46 && -1 != $("#monto").val().indexOf('.'))
                    {
                        bMatch = true;
                    }

                    if (bMatch)
                    {
                        e.preventDefault();
                    }
                });
            });
        </script>
