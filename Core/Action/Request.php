<?php

/**
 * Request.php
 * Archivo que contiene la clase Request
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Action
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Action;

/**
 * Request
 * Clase que se encarga de la gestion de las peticiones
 *
 * @category   AppManager
 * @package    Core
 * @subpackage Action
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class Request
{
    /**
     * Properties
     */
    /**
     * Representa la instancia del objeto que gestiona los POST
     * @var object
     */
    public $oPost;
    /**
     * Representa la instancia del objeto que gestiona los GET
     * @var object
     */
    public $oGet;
    /**
     * Representa la instancia del objeto que gestiona los COOKIE
     * @var object
     */
    public $oCookie;
    /**
     * Representa la instancia del objeto que gestiona los FILES
     * @var object
     */
    public $oFiles;

    /**
     * Methods
     */
    public function __construct()
    {
        $this->_setPostInstance();
        $this->_setGetInstance();
        $this->_setCookieInstance();
        $this->_setFilesInstance();
    }

    /**
     * Metodo setter de la propiedad $oPost
     */
    private function _setPostInstance()
    {
        $this->oPost = \Core\Action\RequestType\Post::getInstance();
    }

    /**
     * Metodo setter de la propiedad $oGet
     */
    private function _setGetInstance()
    {
        $this->oGet = \Core\Action\RequestType\Get::getInstance();
    }

    /**
     * Metodo setter de la propiedad $oCookie
     */
    private function _setCookieInstance()
    {
        $this->oCookie = \Core\Action\RequestType\Cookie::getInstance();
    }

    /**
     * Metodo setter de la propiedad $oCookie
     */
    private function _setFilesInstance()
    {
        $this->oFiles = \Core\Action\RequestType\Files::getInstance();
    }

}