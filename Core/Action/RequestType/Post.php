<?php

/**
 * Post.php
 * Archivo que contiene la clase Post
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Action
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Action\RequestType;

/**
 * Post
 * Clase que se encarga de la gestion de las peticiones POST
 *
 * @category   AppManager
 * @package    Core
 * @subpackage Action
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class Post extends \Core\Action\RequestType\Base
{
    /**
     * Properties
     */
    /**
     * Representa la instancia al objeto
     * @var object
     */
    protected static $oInstance;
    protected static $sClassName = __CLASS__;

    /**
     * Methods
     */
    
    /**
     * Metodo que verifica que hubo una peticion POST
     * 
     * @return bool
     */
    public function requestHasPost()
    {
        if (!empty($_POST)) {
            return true;
        }

        return false;
    }
    
    /**
     * Metodo que obtiene un valor de POST o su totalidad
     *
     * @param string $sKey Contiene los key, separados por punto (.), de manera que se recorran hasta obtener el valor
     * 
     * @return bool
     */
    public function getPost($sKey = '')
    {
        if (!isset($sKey) || empty($sKey)) {
            return $_POST;
        }

        $aKeyParts = explode('.', $sKey);
        $mValue = $_POST;

        $iKeyPartsCount = count($aKeyParts);

        for ($i=0; $i < $iKeyPartsCount; $i++) {
            if (!isset($mValue[$aKeyParts[$i]])) {
                throw new \Exception('No existe $_POST[\'' . $aKeyParts[$i] . '\']');
            }

            $mValue = $mValue[$aKeyParts[$i]];
        }

        return $mValue;
    }

}