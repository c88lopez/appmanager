<?php

/**
 * Get.php
 * Archivo que contiene la clase Get
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Action
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Action\RequestType;

/**
 * Get
 * Clase que se encarga de la gestion de las peticiones GET
 *
 * @category   AppManager
 * @package    Core
 * @subpackage Action
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class Get extends \Core\Action\RequestType\Base
{
    /**
     * Properties
     */
    /**
     * Representa la instancia al objeto
     * @var object
     */
    protected static $oInstance;
    protected static $sClassName = __CLASS__;
    protected $aArguments = array();

    /**
     * Methods
     */
    
    /**
     * Metodo constructor
     */
    public function __construct()
    {
        $this->aArguments = \Core\Router\Url::getUrlArguments();
    }
    
    /**
     * Metodo que verifica que hubo una peticion GET
     * 
     * @return bool
     */
    public function requestHasGet()
    {
        if (!empty($this->aArguments)) {
            return true;
        }

        return false;
    }
    
    /**
     * Metodo que obtiene un valor de GET o su totalidad
     *
     * @param int $iKey Contiene los key
     * 
     * @return bool
     */
    public function getGet($iKey = -1)
    {
        if (-1 === $iKey ) {
            return $this->aArguments;
        }

        return $this->aArguments[$iKey];
    }
    
    /**
     * Metodo valida la existencia de la key del array GET
     *
     * @param int $iKey Contiene los key
     * 
     * @return bool
     */
    public function checkGetKey($iKey)
    {
        return isset($this->aArguments[$iKey]);
    }

}