<?php

/**
 * Files.php
 * Archivo que contiene la clase Files
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Action
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Action\RequestType;

/**
 * Files
 * Clase que se encarga de la gestion de las peticiones FILES
 *
 * @category   AppManager
 * @package    Core
 * @subpackage Action
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class Files extends \Core\Action\RequestType\Base
{
    /**
     * Properties
     */
    /**
     * Representa la instancia al objeto
     * @var object
     */
    protected static $oInstance;
    /**
     * Representa el nombre de la clase
     * @var string
     */
    protected static $sClassName = __CLASS__;
    /**
     * Representa el valor de la propiedad 'name' del campo tipo file que queremos procesar
     */
    protected $sInputFileName;

    /**
     * Methods
     */
    
    /**
     * Metodo que verifica que hubo una subida de archivo
     * 
     * @return bool
     */
    public function requestHasFiles()
    {
        if (!empty($_FILES)) {
            return true;
        }

        return false;
    }

    /**
     * Metodo setter de la propiedad $sInputFileName
     *
     * @param string $sValue El valor del campo a guardar
     *
     * @return void
     */
    public function setInputFileName($sValue)
    {
        $this->sInputFileName = $sValue;
    }

    /**
     * Metodo que retorna el nombre del archivo subido
     *
     * @return string
     */
    public function getUploadedFileName()
    {
        return $this->getUploadedFileData()['name'];
    }

    /**
     * Metodo que retorna el tipo del archivo subido
     *
     * @return string
     */
    public function getUploadedFileType()
    {
        return $this->getUploadedFileData()['type'];
    }

    /**
     * Metodo que retorna la ruta temporal en donde se almaceno el archivo subido
     *
     * @return string
     */
    public function getUploadedFileTmpName()
    {
        return $this->getUploadedFileData()['tmp_name'];
    }

    /**
     * Metodo que retorna el error del archivo subido
     *
     * @return string
     */
    public function getUploadedFileError()
    {
        return $this->getUploadedFileData()['error'];
    }

    /**
     * Metodo que retorna el tamaño del archivo subido
     *
     * @return string
     */
    public function getUploadedFileSize()
    {
        return $this->getUploadedFileData()['size'];
    }

    /**
     * Metodo que devuelve la informacion del proceso de subida del archivo
     * 
     * @return array
     */
    public function getUploadedFileData()
    {
        return $_FILES[$this->sInputFileName];
    }

}