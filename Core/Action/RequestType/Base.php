<?php

/**
 * Base.php
 * Archivo que contiene la clase Base
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Action
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Action\RequestType;

/**
 * Base
 * Clase que posee lo basico que deben poseer las clases que gestionan el tipo especifico de peticion
 * Esta clase posee el patron de diseño 'Singleton'
 *
 * @category   AppManager
 * @package    Core
 * @subpackage Action
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
abstract class Base
{
    /**
     * Properties
     */

    /**
     * Methods
     */
    private function __construct() {}

    /**
     * Metodo que se encarga de retornar una nueva instancia o la ya creada.
     */
    public static function getInstance()
    {
        if (!isset(static::$oInstance)) {
            $sClassName = '\\' . static::$sClassName;
            static::$oInstance = new $sClassName();
        }

        return static::$oInstance;
    }

}