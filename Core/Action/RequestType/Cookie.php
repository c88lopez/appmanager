<?php

/**
 * Cookie.php
 * Archivo que contiene la clase Cookie
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Action
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Action\RequestType;

/**
 * Cookie
 * Clase que se encarga de la gestion de las peticiones COOKIE
 *
 * @category   AppManager
 * @package    Core
 * @subpackage Action
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class Cookie extends \Core\Action\RequestType\Base
{
    /**
     * Properties
     */
    /**
     * Representa la instancia al objeto
     * @var object
     */
    protected static $oInstance;
    protected static $sClassName = __CLASS__;

    /**
     * Methods
     */

}