<?php

/**
 * Json.php
 * Archivo que contiene la clase Json
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Util
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Util;

/**
 * Json
 * Clase que se encarga de gestionar los archivos json
 *
 * @category   AppManager
 * @package    Core
 * @subpackage Util
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class Json
{
    /**
     * Properties
     */

    /**
     * Methods
     */
    /**
     * Metodo que se encarga de parsear el json de un archivo
     *
     * @param string $sJsonPath La ruta del archivo json
     *
     * @return object
     */
    public static function decodeJsonFile($sJsonPath = '')
    {
        if (empty($sJsonPath)) {
            throw new \Exception('La ruta del archivo no puede ser vacio');
        }

        if (false === realpath($sJsonPath)) {
            throw new \Exception('La ruta "' . $sJsonPath . '" no existe');
        }

        return json_decode(
            file_get_contents(
                realpath($sJsonPath)
            )
            , true
        );
    }
}