<?php

/**
 * Log.php
 * Archivo que contiene la clase Log
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Log
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Log;

/**
 * Request
 * Clase que se encarga de la comunicacion cliente <=> servicio
 *
 * @category Core
 * @package  AppManager
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  nolicense No license
 * @link     http://nolink.com
 */
class Log
{
    /**
     * Properties
     */
    static $sMethodLineInfo;

    /**
     * Methods
     */
    
    /**
     * Metodo constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->_initConst();
    }

    /**
     * Metodo que se encargar de instanciar las constantes
     *
     * @return void
     */
    private function _initConst()
    {
        ;
    }
}