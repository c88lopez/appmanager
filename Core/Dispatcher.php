<?php

/**
 * Dispacher.php
 * Archivo que contiene la clase Dispacher
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Dispacher
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core;

/**
 * Dispacher
 * Clase que se encarga de la comunicacion cliente <=> servicio
 *
 * @category Bootstrap
 * @package  AppManager
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  nolicense No license
 * @link     http://nolink.com
 */
class Dispatcher
{
    /**
     * Properties
     */

    /**
     * Methods
     */
    
    /**
     * Dado un request, procede a interpretarlo, procesarlo y responder al cliente.
     * 
     * @return void
     */
    public static function run()
    {
        $aUrlArguments = \Core\Router\Url::getUrlArguments();

        $sAppClass = '\Apps\IndexApps';
        if (!empty($aUrlArguments[0])) {
            /**
             * Se revisa que la aplicacion no este dentro de las aplicaciones excluidas
             */
            if (in_array($aUrlArguments[0], \Core\Validation\Apps::getExcludedApps())) {
                return true;
            }

            /**
             * Se revisa que la aplicacion sea ejecutable
             */
            if (!\Core\Validation\Apps::isExecutable($aUrlArguments[0])) {
                throw new \Exception('La aplicacion \'' . $aUrlArguments[0] . '\' no se puede ejecutar');
            }

            $sAppClass = '\Apps\\' . $aUrlArguments[0] . '\\Main';
        }

        /**
         * Se prepara en un array los valores enviados por GET a la aplicacion
         */
        array_shift($aUrlArguments);
        unset($aUrlArguments);

        /**
         * Se instancia el objeto que gestiona las peticiones
         */
        return (new $sAppClass)->run();
    }

}