<?php

/**
 * Url.php
 * Archivo que contiene la clase Url
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Router
 * @subpackage Url
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Router;

/**
 * Url
 * Clase que se encarga de las operaciones con la URL
 *
 * @category   AppManager
 * @package    Router
 * @subpackage Url
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class Url
{
    /**
     * Properties
     */
    /**
     * Representa el nombre de la carpeta que contiene AppManager
     * @var string
     */
    private static $_sAPFolderName = '';
    /**
     * Representa las partes del url
     * @var array
     */
    private static $_aUrlArguments = array();

    /**
     * Methods
     */

    /**
     * Metodo que se encarga de obtener los parametros
     *
     * @param string $sValue El nombre de la aplicacion a ejecutar
     *
     * @return void
     */
    private static function _setAPFolderName($sValue)
    {
        self::$_sAPFolderName = $sValue;
    }

    /**
     * Metodo getter
     *
     * @return array $_aUrlArguments
     */
    public static function getAPFolderName()
    {
        return self::$_sAPFolderName;
    }

    /**
     * Metodo que se encarga de obtener los parametros
     *
     * @return void
     */
    private static function _setUrlArguments()
    {
        $aScriptFilenameParts = explode(DS, $_SERVER['SCRIPT_FILENAME']);
        end($aScriptFilenameParts);
        self::_setAPFolderName(prev($aScriptFilenameParts));

        self::$_aUrlArguments = self::_sanitizeUrlArguments(
            explode(
                '/', 
                preg_replace(
                    '#([\w\W]*)' . self::$_sAPFolderName . '/#', '', 
                    $_SERVER['REQUEST_URI']
                )
            )
        );
    }

    /**
     * Metodo getter
     *
     * @return array $_aUrlArguments
     */
    public static function getUrlArguments()
    {
        if (empty(self::$_aUrlArguments)) {
            self::_setUrlArguments();
        }
        return self::$_aUrlArguments;
    }

    /**
     * Metodo que se encarga de sanar los datos que llegan por URL
     * 
     * @param array $aValues Array que contiene los argumentos de AP
     * 
     * @return array
     */
    private static function _sanitizeUrlArguments($aValues)
    {
        $iCantValues = count($aValues);
        for ($i=0; $i < $iCantValues; $i++) {
            if (empty($aValues[$i])) {
                // Evito que, dada un URL con '/' final, se genere un argumento vacio
                unset($aValues[$i]);
            }
        }

        return $aValues;
    }

    /**
     * Metodo que retorna la URL inicio de la aplicacion
     *
     * @return string
     */
    public static function getInitUrl()
    {
        $sInitUrl = 'http://';
        if (empty($_SERVER['SERVER_NAME'])) {
            throw new \Exception('SERVER_NAME esta vacio');
        }
        $sInitUrl .= $_SERVER['SERVER_NAME'];

        if (empty($_SERVER['PHP_SELF'])) {
            throw new \Exception('PHP_SELF esta vacio');
        }
        $sPhpSelf = $_SERVER['PHP_SELF'];
        $aPhpSelfParts = explode(DS, $sPhpSelf);
        array_pop($aPhpSelfParts);

        $sInitUrl .= implode(DS, $aPhpSelfParts);
        return $sInitUrl;
    }

    /**
     * Metodo que se encarga de redirigir al inicio
     *
     * @return bool
     */
    public static function redirectInit()
    {
        header("Location: " . self::getInitUrl());
        return true;
    }
}