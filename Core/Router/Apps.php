<?php

/**
 * Apps.php
 * Archivo que contiene la clase Apps
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Router
 * @subpackage Apps
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Router;

/**
 * Apps
 * Clase que se encarga de las operaciones sobre rutas y caminos relacionado con las aplicaciones
 *
 * @category   AppManager
 * @package    Router
 * @subpackage Apps
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class Apps
{
    /**
     * Properties
     */
    

    /**
     * Methods
     */
    
    /**
     * Metodo que retorna la lista de aplicaciones disponibles con informacion referent a cada una
     * @return array
     */
    public static function getAvailableApps()
    {
        $aAppList = array();

        $oRDI = new \RecursiveDirectoryIterator(PATH_AP_APPS);
        $oRDI->setFlags(\RecursiveDirectoryIterator::SKIP_DOTS);
        $i = 0;
        foreach ($oRDI as $sAppPath => $oSFI) {
            if ($oSFI->isDir()) {
                $aAppPathParts = explode(DS, $sAppPath);
                /**
                 * Se valida que la aplicacion encontrada pueda ser ejecutable.
                 */
                if (!\Core\Validation\Apps::isExecutable(end($aAppPathParts))) {
                    continue;
                }

                /**
                 * Parseo el archivo .json que contiene informacion de la aplicacion
                 */
                $oAppInfo = \Core\Util\Json::decodeJsonFile(
                    realpath($sAppPath) . DS . 'AppInfo.json'
                );

                /**
                 * Recompilo informacion de la aplicacion
                 */
                $aAppList[$i]['title']       = $oAppInfo['title'];
                $aAppList[$i]['description'] = $oAppInfo['description'];
                $aAppList[$i++]['path']      = end($aAppPathParts);
            }
        }

        usort($aAppList, array(__CLASS__, 'compareAppName'));
        return $aAppList;
    }

    /**
     * Metodo que tiene la logica para decidir cual de los dos elementos es mayor
     *
     * @param array $a Representa el primer array a comprar
     * @param array $b Representa el segundo array a comprar
     *
     * @return integer
     */
    private static function compareAppName($a, $b)
    {
        return (strtolower($a['title']) < strtolower($b['title'])) ? -1 : 1;
    }

    /**
     * Metodo que se encarga de obtener la URL en base a la app ingresada
     *
     * @param string $sAppName   El nombre de la aplicacion a calcular
     * @param string $aArguments El listado de argumentos a enviar por GET
     *
     * @return string
     */
    public static function getUrlByApp($sAppName, $aArguments = array())
    {
        if (empty($sAppName)) {
            throw new \Exception('El nombre de la aplicacion no debe estar vacio');
        }

        if (!\Core\Validation\Apps::appExists($sAppName)) {
            throw new \Exception('La aplicacion "' . $sAppName . '" no existe');
        }

        $sUrl = \Core\Router\Url::getInitUrl() . DS . $sAppName;

        if (!empty($aArguments)) {
            $sUrl .= DS . implode(DS, $aArguments);
        }

        return $sUrl;
    }

    /**
     * Metodo que se encarga de obtener la URL en base a la app ingresada
     *
     * @param string $sAppName El nombre de la aplicacion a calcular
     *
     * @return string
     */
    public static function redirectUrlByApp($sAppName, $aArguments = array())
    {
        header("Location: " . self::getUrlByApp($sAppName, $aArguments));
    }
}