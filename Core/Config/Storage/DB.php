<?php

/**
 * Archivo que contiene la clase Config Db
 *
 * PHP Version 5
 *
 * @category   AppManager
 * @package    Config
 * @subpackage Storage
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Config\Storage;

/**
 * Class Config
 *
 * @category   AppManager
 * @package    Config
 * @subpackage Storage
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class DB
{
    /**
     * Properties
     */
    public static $aConnection = array(
        'host'     => 'localhost',
        'dbname'   => 'AppManager',
        'user'     => 'root',
        'password' => 'clopez.-88',
        'driver'   => 'mysql',
    );

    /**
     * Methods
     */
    

}