<?php

/**
 * Archivo que contiene la clase abstracta Base
 *
 * PHP Version 5
 *
 * @category AppManager
 * @package  Base
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  nolicense No license
 * @link     http://nolink.com
 */
namespace Core\Config;

/**
 * Class Base
 *
 * @category AppManager
 * @package  Base
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  nolicense No license
 * @link     http://nolink.com
 */
abstract class Base
{
    /**
     * Properties
     */
    
    /**
     * Methods
     */
    
}