<?php

/**
 * Archivo que contiene la clase jQueryUIJs
 *
 * PHP Version 5
 *
 * @category   AppManager
 * @package    Config
 * @subpackage Template
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Config\Template\Connectors;

/**
 * Class jQueryUIJs
 *
 * @category   AppManager
 * @package    Config
 * @subpackage Template
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class jQueryUIJs
{
    /**
     * Properties
     */
    protected static $sType = '';
    protected static $sSrc  = '';
    
    /**
     * Methods
     */
    
    /**
     * Metodo que retorna el objeto conector elegido
     * 
     * @return object
     */
    public static function getType()
    {
        return 'text/javascript';
    }
    
    /**
     * Metodo que retorna el objeto conector elegido
     * 
     * @return object
     */
    public static function getSrc()
    {
        return \Core\Router\Url::getInitUrl() . DS . 
            'Vendor' . DS . 
            'jQueryUI' . DS . 
            'js' . DS . 
            'jquery-ui.min.js';
    }
    
}