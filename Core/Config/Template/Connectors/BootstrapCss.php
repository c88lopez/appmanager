<?php

/**
 * Archivo que contiene la clase Config
 *
 * PHP Version 5
 *
 * @category   AppManager
 * @package    Config
 * @subpackage Config
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Config\Template\Connectors;

/**
 * Class Config
 *
 * @category   AppManager
 * @package    Config
 * @subpackage Config
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class BootstrapCss
{
    /**
     * Properties
     */
    protected static $sHref  = '';
    protected static $sRel   = '';
    protected static $sMedia = '';

    /**
     * Methods
     */
    /**
     * Metodo que retorna el objeto conector elegido
     * 
     * @return object
     */
    public static function getHref()
    {
        return \Core\Router\Url::getInitUrl() . DS . 
            'Vendor' . DS . 
            'Bootstrap' . DS . 
            'css' . DS . 
            'bootstrap.min.css';
    }

    /**
     * Metodo que retorna el objeto conector elegido
     * 
     * @return object
     */
    public static function getRel()
    {
        return 'stylesheet';
    }

    /**
     * Metodo que retorna el objeto conector elegido
     * 
     * @return object
     */
    public static function getMedia()
    {
        return 'screen';
    }

}