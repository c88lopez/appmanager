<?php

/**
 * Archivo que contiene la clase jQueryUICss
 *
 * PHP Version 5
 *
 * @category   AppManager
 * @package    Config
 * @subpackage Template
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Config\Template\Connectors;

/**
 * Class Config
 *
 * @category   AppManager
 * @package    Config
 * @subpackage Template
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class jQueryUICss
{
    /**
     * Properties
     */
    protected static $sHref  = '';
    protected static $sRel   = '';
    protected static $sMedia = '';

    /**
     * Methods
     */
    /**
     * Metodo que retorna el objeto conector elegido
     * 
     * @return object
     */
    public static function getHref()
    {
        return \Core\Router\Url::getInitUrl() . DS . 
            'Vendor' . DS . 
            'jQueryUI' . DS . 
            'css' . DS . 
            'jquery-ui.min.css';
    }

    /**
     * Metodo que retorna el objeto conector elegido
     * 
     * @return object
     */
    public static function getRel()
    {
        return 'stylesheet';
    }

    /**
     * Metodo que retorna el objeto conector elegido
     * 
     * @return object
     */
    public static function getMedia()
    {
        return 'screen';
    }

}