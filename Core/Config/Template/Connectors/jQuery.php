<?php

/**
 * Archivo que contiene la clase jQuery
 *
 * PHP Version 5
 *
 * @category   AppManager
 * @package    Config
 * @subpackage jQuery
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Config\Template\Connectors;

/**
 * Class jQuery
 *
 * @category   AppManager
 * @package    Config
 * @subpackage jQuery
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class jQuery
{
    /**
     * Properties
     */
    protected static $sType = '';
    protected static $sSrc  = '';
    
    /**
     * Methods
     */
    
    /**
     * Metodo que retorna el objeto conector elegido
     * 
     * @return object
     */
    public static function getType()
    {
        return 'text/javascript';
    }
    
    /**
     * Metodo que retorna el objeto conector elegido
     * 
     * @return object
     */
    public static function getSrc()
    {
        return \Core\Router\Url::getInitUrl() . DS . 
            'Vendor' . DS . 
            'jQuery' . DS . 
            'jQuery.min.js';
    }
    
}