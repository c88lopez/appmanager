<?php

/**
 * Archivo que contiene la clase Config
 *
 * PHP Version 5
 *
 * @category   AppManager
 * @package    Config
 * @subpackage Config
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Config\Template\Connectors;

/**
 * Class Config
 *
 * @category   AppManager
 * @package    Config
 * @subpackage Config
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class BootstrapJs
{
    /**
     * Properties
     */
    protected static $sType = '';
    protected static $sSrc  = '';
    
    /**
     * Methods
     */
    
    /**
     * Metodo que retorna el objeto conector elegido
     * 
     * @return object
     */
    public static function getType()
    {
        return 'text/javascript';
    }
    
    /**
     * Metodo que retorna el objeto conector elegido
     * 
     * @return object
     */
    public static function getSrc()
    {
        return \Core\Router\Url::getInitUrl() . DS . 
            'Vendor' . DS . 
            'Bootstrap' . DS . 
            'js' . DS . 
            'bootstrap.min.js';
    }
    
}