<?php

/**
 * Archivo que contiene la clase Config Engine
 *
 * PHP Version 5
 *
 * @category   AppManager
 * @package    Config
 * @subpackage Template
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Config\Template;

/**
 * Class Config
 *
 * @category   AppManager
 * @package    Config
 * @subpackage Template
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class Engine
{
    /**
     * Properties
     */
    /**
     * Representa el nombre del conector del template
     * @var string
     */
    protected static $aConnectors = array(
        'css' => array(
            'BootstrapCss',
            'jQueryUICss',
        ),
        'js' => array(
            'jQuery',
            'BootstrapJs',
            'jQueryUIJs',
        ),
    );
    /**
     * Representa el nombre del conector del template
     * @var string
     */
    protected static $sHeadTagTitle = 'AppManager';
    
    /**
     * Methods
     */
    
    /**
     * Metodo que retorna el objeto conector elegido
     * 
     * @return object
     */
    public static function getConnectors()
    {
        return self::$aConnectors;
    }
    
    /**
     * Metodo que retorna el objeto conector elegido
     * 
     * @return object
     */
    public static function getHeadTagTitle()
    {
        return self::$sHeadTagTitle;
    }
}