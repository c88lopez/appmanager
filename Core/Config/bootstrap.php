<?php

/**
 * bootstrap.php
 * Archivo que carga las dependencias de la aplicacion
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Config
 * @subpackage Bootstrap
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */

/**
 * Carga de constantes
 */
define('DS', DIRECTORY_SEPARATOR);
define('PATH_AP', realpath(dirname(dirname(__DIR__))));
define('PATH_AP_APPS', PATH_AP . DS . 'Apps');
define('PATH_AP_CORE', PATH_AP . DS . 'Core');
define('PATH_AP_TEMPLATES', PATH_AP . DS . 'Templates');
define('PATH_AP_VENDOR', PATH_AP . DS . 'Vendor');

/**
 * Se carga el autoload para las clases de la aplicacion
 */
require_once 'autoload.php';
$oSCL = new SplClassLoader();
$oSCL->register();

/**
 * Se lanza la aplicacion
 */
\Core\Dispatcher::run();
