<?php

/**
 * Engine.php
 * Archivo que contiene la clase Engine
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Template
 * @subpackage Engine
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Template;

/**
 * Engine
 * Clase que se encarga de los templates y mostrarlos en pantalla
 *
 * @category   AppManager
 * @package    Template
 * @subpackage Engine
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class Engine
{
    /**
     * Properties
     */
    /**
     * Representa el sufijo del archivo template
     * @var string
     */
    private static $_sSuffix = 'Tpl';
    /**
     * Representa la extension del archivo
     * @var string
     */
    private static $_sExtension = 'php';
    /**
     * Representa el conector de estilos
     * @var object
     */
    private static $_sCssConnectors = array();
    /**
     * Representa el conector de javascript
     * @var object
     */
    private static $_sJsConnectors = array();

    /**
     * Methods
     */
    
    /**
     * Metodo que se encarga de configurar a twig y correr el metodo que corresponda
     *
     * @param string $sClassMethod El valor de formato __METHOD__
     * 
     * @return bool
     */
    public static function render($sClassMethod, $aData = array())
    {
        if (!empty($aData)) {
            foreach ($aData as $sVarName => $sVarValue) {
                $$sVarName = $sVarValue;
            }
        }

        /**
         * Se cargan los conectores especificados en la configuracion
         */
        self::_setConnectors();

        include self::_getHtmlHead();
        include self::_getHtmlNavBar();
        include self::_getTemplatePath($sClassMethod);
        include self::_getHtmlClose();

        /**
         * Se limpian los mensajes enviados al usuario
         */
        self::_flushSessionMessages();

        /**
         * Se limpian los datos retenidos de un formulario de existir
         */
        self::_flushFormData();

        return true;
    }

    /**
     * Metodo que retorna el titulo de la aplicacion
     *
     * @return string
     */
    public static function getTitleHeadText()
    {
        return \Core\Config\Template\Engine::getHeadTagTitle();
    }

    /**
     * Metodo que retorna el listado bien formatedo de la etiqueta <script> para los archivos javascript
     *
     * @param string $sTagType El tipo de tag a cartar [css|js]
     *
     * @return string
     */
    public static function getHtmlTags($sTagType = '')
    {
        if (empty($sTagType)) {
            throw new \Exception("El tipo de tag debe especificarse");
        }
        $sVarName = '_s' . ucfirst($sTagType) . 'Connectors';
        $sPropertyValue = get_class_vars(__CLASS__)[$sVarName];

        $aHtmlScriptList = array();
        $iConnectorsCount = count($sPropertyValue);

        for ($i=0; $i < $iConnectorsCount; $i++) {
            $sConnectorClass = $sPropertyValue[$i];

            $aHtmlScriptList[] = $sConnectorClass::getHtmlInclude();
        }

        return implode("\n", $aHtmlScriptList);
    }

    /**
     * Metodo que configura un conector para el template
     *
     * @return bool
     */
    private static function _setConnectors()
    {
        $aConnectorsList = \Core\Config\Template\Engine::getConnectors();
        $iCssConnectorsCount = count($aConnectorsList['css']);
        $iJsConnectorsCount  = count($aConnectorsList['js']);

        for ($i=0; $i < $iCssConnectorsCount; $i++) {
            $sConnectorClass = '\Core\Template\Connectors\\' . $aConnectorsList['css'][$i];
            self::$_sCssConnectors[] = $sConnectorClass;
        }

        for ($i=0; $i < $iJsConnectorsCount; $i++) {
            $sConnectorClass = '\Core\Template\Connectors\\' . $aConnectorsList['js'][$i];
            self::$_sJsConnectors[] = $sConnectorClass;
        }

        return true;
    }

    /**
     * Metodo que retorna la ruta absoluta del template a utilizar
     * 
     * @param string $sClassMethod La clase y metodo ejecutados que requieren un template
     * 
     * @return string
     */
    private static function _getTemplatePath($sClassMethod)
    {
        $sTemplatePath = realpath(
            PATH_AP_TEMPLATES . 
            DS . 
            self::_parseClassMethod($sClassMethod) . 
            self::$_sSuffix . 
            '.' . 
            self::$_sExtension
        );

        if (!$sTemplatePath) {
            throw new \Exception('No existe un archivo template para el metodo ' . $sClassMethod);
        }

        return $sTemplatePath;
    }

    /**
     * Metodo que retorna la ruta absoluta del template a abre el html
     * 
     * @return string
     */
    private static function _getHtmlHead()
    {
        return realpath(
            PATH_AP_TEMPLATES . 
            DS . 
            'Common' . 
            DS . 
            'HtmlOpen' .
            '.' .
            self::$_sExtension
        );
    }

    /**
     * Metodo que retorna la ruta absoluta del template de la bara de navegacion
     * 
     * @return string
     */
    private static function _getHtmlNavBar()
    {
        return realpath(
            PATH_AP_TEMPLATES . 
            DS . 
            'Common' . 
            DS . 
            'HtmlNavBar' .
            '.' .
            self::$_sExtension
        );
    }

    /**
     * Metodo que retorna la ruta absoluta del template que cierra el html
     * 
     * @return string
     */
    private static function _getHtmlClose()
    {
        return realpath(
            PATH_AP_TEMPLATES . 
            DS . 
            'Common' . 
            DS . 
            'HtmlClose' .
            '.' .
            self::$_sExtension
        );
    }

    /**
     * Metodo que limpia los mensajes al usuario de la sesion
     * 
     * @return bool
     */
    private static function _flushSessionMessages()
    {
        return \Core\Storage\Session::flushMessages();
    }

    /**
     * Metodo que limpia los datos retenidos de un formulario que fallo su validacion
     * 
     * @return bool
     */
    private static function _flushFormData()
    {
        return \Core\Storage\Session::flushFormData();
    }

    /**
     * Metodo que se encarga de parsear la informacion proveniente de __METHOD__
     * 
     * @param string $sClassMethod El valor recibido de __METHOD__
     * 
     * @return string
     */
    private static function _parseClassMethod($sClassMethod)
    {
        $aClassMethodParts = explode('::', $sClassMethod);
        // Revisamos si el metodo invocado es privado. De ser correcto, se quita el guion bajo
        if (false !== stripos($aClassMethodParts[1], '_')) {
            $aClassMethodParts[1] = str_replace('_', '', $aClassMethodParts[1]);
        }
        return preg_replace('/\\\|::/', DS, implode('::', $aClassMethodParts));
    }
}
