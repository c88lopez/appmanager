<?php

/**
 * Jquery.php
 * Archivo que contiene la clase Jquery
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Template
 * @subpackage Jquery
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Template\Connectors;

/**
 * Jquery
 * Clase que conecta al template con la libreria jQuery
 *
 * @category   AppManager
 * @package    Template
 * @subpackage Jquery
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class BootstrapJs implements \Core\Template\Connectors\Base
{
    /**
     * Properties
     */
    

    /**
     * Methods
     */
    /**
     * Metodo  que retorna el html correspondiente para que se incluya su contenido
     *
     * @return string
     */
    public static function getHtmlInclude()
    {
        $sHtml = "\t\t";
        $sHtml .= '<script ';
        $sHtml .= 'type="' . \Core\Config\Template\Connectors\BootstrapJs::getType() . '" ';
        $sHtml .= 'src="' . \Core\Config\Template\Connectors\BootstrapJs::getSrc() . '" ';
        $sHtml .= '></script>';

        return $sHtml;
    }

}