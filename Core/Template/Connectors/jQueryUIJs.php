<?php

/**
 * jQueryUIJs.php
 * Archivo que contiene la clase jQueryUIJs
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Template
 * @subpackage jQueryUI
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Template\Connectors;

/**
 * jQueryUIJs
 * Clase que conecta al template con la libreria jQueryUI
 *
 * @category   AppManager
 * @package    Template
 * @subpackage jQueryUI
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class jQueryUIJs implements \Core\Template\Connectors\Base
{
    /**
     * Properties
     */
    

    /**
     * Methods
     */
    /**
     * Metodo  que retorna el html correspondiente para que se incluya su contenido
     *
     * @return string
     */
    public static function getHtmlInclude()
    {
        $sHtml = "\t\t";
        $sHtml .= '<script ';
        $sHtml .= 'type="' . \Core\Config\Template\Connectors\jQueryUIJs::getType() . '" ';
        $sHtml .= 'src="' . \Core\Config\Template\Connectors\jQueryUIJs::getSrc() . '" ';
        $sHtml .= '></script>';

        return $sHtml;
    }

}