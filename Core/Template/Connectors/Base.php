<?php

/**
 * Base.php
 * Archivo que contiene la clase abstracta Base
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Template
 * @subpackage Base
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Template\Connectors;

/**
 * Base
 * Clase que contiene lo basico para los conectores
 *
 * @category   AppManager
 * @package    Template
 * @subpackage Base
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
interface Base
{
    /**
     * Properties
     */
    

    /**
     * Methods
     */

    /**
     * Metodo  que retorna el html correspondiente para que se incluya su contenido
     *
     * @return string
     */
    public static function getHtmlInclude();

}