<?php

/**
 * jQueryUICss.php
 * Archivo que contiene la clase jQueryUICss
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Template
 * @subpackage jQueryUI
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Template\Connectors;

/**
 * jQueryUICss
 * Clase que conecta al template con la libreria jQueryUI
 *
 * @category   AppManager
 * @package    Template
 * @subpackage jQueryUI
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class jQueryUICss implements \Core\Template\Connectors\Base
{
    /**
     * Properties
     */

    /**
     * Methods
     */
    /**
     * Metodo  que retorna el html correspondiente para que se incluya su contenido
     *
     * @return string
     */
    public static function getHtmlInclude()
    {
        $sHtml = '<link ';
        $sHtml .= 'href="' . \Core\Config\Template\Connectors\jQueryUICss::getHref() . '" ';
        $sHtml .= 'rel="' . \Core\Config\Template\Connectors\jQueryUICss::getRel() . '" ';
        $sHtml .= 'media="' . \Core\Config\Template\Connectors\jQueryUICss::getMedia() . '" ';
        $sHtml .= '/>';
        $sHtml .= "\n";

        return $sHtml;
    }

}