<?php

/**
 * Bootstrap.php
 * Archivo que contiene la clase Bootstrap
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Template
 * @subpackage Bootstrap
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Template\Connectors;

/**
 * Bootstrap
 * Clase que conecta al template con la libreria bootstrap
 *
 * @category   AppManager
 * @package    Template
 * @subpackage Bootstrap
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class BootstrapCss implements \Core\Template\Connectors\Base
{
    /**
     * Properties
     */

    /**
     * Methods
     */
    /**
     * Metodo  que retorna el html correspondiente para que se incluya su contenido
     *
     * @return string
     */
    public static function getHtmlInclude()
    {
        $sHtml = '<link ';
        $sHtml .= 'href="' . \Core\Config\Template\Connectors\BootstrapCss::getHref() . '" ';
        $sHtml .= 'rel="' . \Core\Config\Template\Connectors\BootstrapCss::getRel() . '" ';
        $sHtml .= 'media="' . \Core\Config\Template\Connectors\BootstrapCss::getMedia() . '" ';
        $sHtml .= '/>';
        $sHtml .= "\n";

        return $sHtml;
    }

}