<?php

/**
 * Db.php
 * Archivo que contiene la clase Db
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Storge
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Storage;

/**
 * Db
 * Clase que se encarga de gestionar la informacion en la sesion
 *
 * @category   AppManager
 * @package    Core
 * @subpackage Storge
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class DB extends \Core\Storage\Base
{
    /**
     * Properties
     */
    /**
     * Representa la instancia de la base de datos
     * @var object
     */
    protected $oDB;
    /**
     * Representa la sentencia de la consulta ejecutada
     * @var object
     */
    protected $oStmt;

    /**
     * Methods
     */
    /**
     * Metodo constructor
     */
    public function __construct()
    {
        $this->oDB = $this->connectDB();
    }

    /**
     * Metodo que se encarga de conectarse a la base de datos con los datos especificados
     *
     * @return object
     */
    public function connectDB()
    {
        $aConnection = \Core\Config\Storage\DB::$aConnection;
        return new \PDO(
            $aConnection['driver'] . ':' . 
            'host=' . $aConnection['host'] . ';' . 
            'dbname=' . $aConnection['dbname'], 
            $aConnection['user'],
            $aConnection['password']
        );
    }

    /**
     * Methodo que ejecuta una consulta y retorna un array del tipo indicado
     * 
     * @param string $sSql      La consulta a generar
     * @param array  $aBindings Los valores a utilizar en la consulta
     * @param string $iType     El tipo de array retornado
     * 
     * @return array
     */
    public function queryFetchAll($sSql, $aBindings = array(), $iType = \PDO::FETCH_ASSOC)
    {
        $this->executeSql($sSql, $aBindings);

        return $this->oStmt->fetchAll($iType);
    }

    /**
     * Methodo que ejecuta una consulta y retorna un array del tipo indicado
     * 
     * @param string $sSql La consulta a generar
     * @param array  $aBindings Los valores a utilizar en la consulta
     * 
     * @return array
     */
    public function queryExecute($sSql, $aBindings = array())
    {
        $this->executeSql($sSql, $aBindings);

        return $this->oStmt->rowCount();
    }

    /**
     * Methodo que ejecuta una consulta y retorna un array del tipo indicado
     * 
     * @param string $sFunction  La funcion PL/SQL a ejecutar.
     * @param array  $mArguments Los valores a utilizar en la consulta.
     * @param bool   $bFetch     Indica si se deben obtener los valores luego de ejecutar la consulta.
     * 
     * @return array
     */
    public function callFunction($sFunction, $mArguments = array(), $bFetch = false)
    {
        if (!is_array($mArguments)) {
            $mArguments = array($mArguments);
        }

        $sSql = $this->buildCallQuery($sFunction, $mArguments);
        $this->executeSql($sSql, $mArguments);

        if ($bFetch) {
            $mResult = $this->oStmt->fetchAll(\PDO::FETCH_ASSOC);
        } else {
            $mResult = $this->oStmt->rowCount();
        }

        return $mResult;
    }

    /**
     * Metodo que se encarga de construir la consulta la base de datos
     * 
     * @param string $sFunction La funcion a ejecutar
     * @param array  $aBindings Los valores a pasar como argumentos
     * 
     * @return string
     */
    protected function buildCallQuery($sFunction, $aArguments = array())
    {
        $sSql = 'CALL ' . $sFunction . '(';
        foreach ($aArguments as $key => $value) {
            $aArguments[$key] = '?';
        }
        $sSql .= implode(',', $aArguments) . ')';

        return $sSql;
    }

    /**
     * Metodo que prepara y ejecuta una consulta almacenando internamente la declaracion de dicho resultado
     * Si se le provee un array no asociativo, utilizara los bindings con signos de interrogacion
     * 
     * @param string $sSql      La consulta a ejecutar
     * @param array  $aBindings Los valores a utilizar
     * 
     * @return bool
     */
    protected function executeSql($sSql, $aBindings = array())
    {
        $oStmt = $this->oDB->prepare($sSql);

        /**
         * Almaceno la declaracion de la consulta en el objeto DB
         */
        $this->oStmt =& $oStmt;

        /**
         * Valido que el array ingresado sea asociativo, para luego decidir por cual metodo entrar
         */
        $iBindingsCount = count($aBindings);
        if (0 !== $iBindingsCount) {
            if (array_keys($aBindings) !== range(0, $iBindingsCount-1)) {
                $this->prepareBindings($aBindings);
            } else {
                $this->prepareQuestionBindings($aBindings);
            }
        }

        $this->oStmt->execute();

        /**
         * Capturo posibles errores
         */
        $this->handlerError();

        return true;
    }

    /**
     * Metodo que prepara los bindings
     *
     * @param array $aBindings Los enlaces de los valores
     *
     * @return bool
     */
    protected function prepareBindings($aBindings)
    {
        reset($aBindings);
        while($mData = current($aBindings)) {
            $this->oStmt->bindValue(key($aBindings), $mData);
            next($aBindings);
        }

        return true;
    }

    /**
     * Metodo que prepara los bindings de signos de interrogacion
     *
     * @param array $aBindings Los enlaces de los valores
     *
     * @return bool
     */
    protected function prepareQuestionBindings($aBindings)
    {
        reset($aBindings);
        while($mData = current($aBindings)) {

            $this->oStmt->bindValue(key($aBindings)+1, $mData);

            next($aBindings);
        }
        return true;
    }

    /**
     * Metodo que captura los errores
     *
     * @return bool
     */
    protected function handlerError()
    {
        /**
         * Esta es la condicion para que exista errores
         */
        if ('00000' !== $this->oStmt->errorCode()) {
            $sErrorCode = $this->oStmt->errorCode();
            $sErrorInfo = $this->oStmt->errorInfo()[2];

            $sErrorMessage = <<<EOL
Ocurrio un error al ejecutar la consulta. Codigo: %d Mensaje: %s
EOL;
            throw new \Exception(sprintf($sErrorMessage, $sErrorCode, $sErrorInfo));
        }

        return true;
    }

}