<?php

/**
 * Base.php
 * Archivo que contiene la clase Base
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Storage
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Storage;

/**
 * Base
 * Clase que contiene lo basico de una clase de almacenamiento
 *
 * @category   AppManager
 * @package    Core
 * @subpackage Storage
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class Base
{
    /**
     * Properties
     */

    /**
     * Methods
     */
    public function __construct() {}

}