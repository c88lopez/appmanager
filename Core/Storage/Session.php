<?php

/**
 * Session.php
 * Archivo que contiene la clase Session
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Storge
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Storage;

/**
 * Session
 * Clase que se encarga de gestionar la informacion en la sesion
 *
 * @category   AppManager
 * @package    Core
 * @subpackage Storge
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class Session extends \Core\Storage\Base
{
    /**
     * Properties
     */
    /**
     * Representa el valor de la key donde se guardan los mensajes
     */
    const MSG_SESSION_KEY = 'messages';
    /**
     * Representa el valor de la key donde se guardan los datos del formulario cuya validacion fallo
     */
    const MSG_FORM_DATA_KEY = 'formData';
    /**
     * Esta lista de constantes se utilizan para definir el tipo de mensaje a mostrar
     */
    const MSG_LEVEL_SUCCESS = 'success';
    const MSG_LEVEL_INFO    = 'info';
    const MSG_LEVEL_WARNING = 'warning';
    const MSG_LEVEL_ERROR   = 'danger';

    /**
     * Methods
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Metodo que se encarga de iniciar la sesion
     *
     * @return bool
     */
    public function start()
    {
        return session_start();
    }

    /**
     * Metodo que se encarga de establer u obtener el id de la sesion actual
     *
     * @return string
     */
    public function getID($sID = '')
    {
        if ('' === $sID) {
            return session_id();
        } else {
            return session_id($sID);
        }
    }

    /**
     * Metodo que se encarga de devolver el estado de la sesion actual
     *
     * @return bool
     */
    public function getStatus()
    {
        return session_status();
    }

    /**
     * Metodo que se encarga de guardar un valor en la key proporcionada
     * Se puede especificar la asociatividad separando por puntos cada string.
     * (key1.key2.key3) => $_SESSION['key1']['key2']['key3']
     * 
     * @param string $sKey   La key del array donde se guardara el valor
     * @param mixed  $mValue El valor a guardar
     * 
     * @return bool
     */
    public function setValue($sKey, $mValue)
    {
        if (empty($sKey)) {
            throw new \Exception('La clave no puede ser un string vacio');
        }

        $aSession =& $_SESSION;
        /**
         * Me posiciona en la key del array donde se guardaran el valor ingresado
         */
        $aKeyParts = $this->_getKeyParts($sKey);
        $iKeyPartsCount = count($aKeyParts);
        for ($i=0; $i < $iKeyPartsCount; $i++) {
            $sFirstPartKey = array_shift($aKeyParts);
            if (isset($aSession[$sFirstPartKey])) {
                $aSession =& $aSession[$sFirstPartKey];
            } else {
                break;
            }
        }

        $aSession[$sFirstPartKey] = $this->_generateSessionArray($aKeyParts, $mValue);

        return true;
    }

    /**
     * Metodo que se encarga de obtener el valor en la key proporcionada
     * Se puede especificar la asociatividad separando por puntos cada string.
     * (key1.key2.key3) => $_SESSION['key1']['key2']['key3']
     * 
     * @param string $sKey La key a obtener
     * 
     * @return bool
     */
    public function getValue($sKey)
    {
        if (empty($sKey)) {
            throw new \Exception('La clave no puede ser un string vacio');
        }

        $mValue = null;
        $aSession =& $_SESSION;

        $aKeyParts = $this->_getKeyParts($sKey);
        $iKeyPartsCount = count($aKeyParts);
        for ($i=0; $i < $iKeyPartsCount; $i++) {
            if (($i+1) >= $iKeyPartsCount) {
                $mValue = $aSession[$aKeyParts[$i]];
                break;
            }

            $aSession =& $aSession[$aKeyParts[$i]];
        }

        return $mValue;
    }

    /**
     * Metodo que verifica la existencia de una key
     * 
     * @param string $sKey La key a validar
     * 
     * @return bool
     */
    public function checkKey($sKey)
    {
        $bExists = true;
        $aSession =& $_SESSION;

        $aKeyParts = $this->_getKeyParts($sKey);
        $iKeyPartsCount = count($aKeyParts);
        for ($i=0; $i < $iKeyPartsCount; $i++) {
            if (isset($aSession[$aKeyParts[$i]])) {
                $aSession =& $aSession[$aKeyParts[$i]];
            } else {
                $bExists = false;
            }
        }

        return $bExists;
    }

    /**
     * Metodo que limpia una key de la sesion
     * 
     * @param string $sKey El nombre de la key a limpiar
     * 
     * @return bool
     */
    public function unsetKey($sKey)
    {
        if (empty($sKey)) {
            throw new \Exception('La key no puede ser un string vacio');
        }

        $aKeyParts = $this->_getKeyParts($sKey);
        $iKeyPartsCount = count($aKeyParts);

        $aSession =& $_SESSION;

        for($i=0 ; $i < $iKeyPartsCount ; $i++) {
            if (($i+1) >= $iKeyPartsCount) {
                unset($aSession[$aKeyParts[$i]]);
                break;
            }

            $aSession =& $aSession[$aKeyParts[$i]];
        }

        return true;
    }

    /**
     * Metodo guarda un mensaje que sera mostrado al usuario y eliminado de la sesion
     * 
     * @param string $sMessage Representa el mensaje a mostrar
     * @param string $sLevel   Representa el nivel de atencion del mensaje
     *
     * @return bool
     */
    public function addMessage($sMessage, $sLevel = self::MSG_LEVEL_ERROR)
    {
        if (empty($sMessage)) {
            throw new \Exception('El mensaje no puede ser un string vacio.');
        }

        $aSession =& $_SESSION;
        $aSession[self::MSG_SESSION_KEY][$sLevel][] = $sMessage;

        return true;
    }

    /**
     * Metodo que obtiene los mensajes guardados
     * 
     * @return array
     */
    public static function getMessages()
    {
        $aReturn = array();

        $aSession =& $_SESSION;

        if (isset($aSession[self::MSG_SESSION_KEY])) {
            $aReturn = $aSession[self::MSG_SESSION_KEY];
        }

        return $aReturn;
    }

    /**
     * Metodo que limpia los mensajes almacenados en sesion que se muestran en pantalla
     * 
     * @return bool
     */
    public static function flushMessages()
    {
        if (isset($_SESSION[self::MSG_SESSION_KEY])) {
            unset($_SESSION[self::MSG_SESSION_KEY]);
        }

        return true;
    }

    /**
     * Metodo guarda un mensaje que sera mostrado al usuario y eliminado de la sesion
     * 
     * @param array $aFieldValue Representa un array asociativo donde la key es el nombre del field y su valor
     * 
     * @return bool
     */
    public function addFormData($aFieldValue)
    {
        if (empty($aFieldValue)) {
            throw new \Exception('La lista de valores no puede ser un array vacio.');
        }

        $aSession =& $_SESSION;
        $aSession[self::MSG_FORM_DATA_KEY] = $aFieldValue;

        return true;
    }

    /**
     * Metodo que obtiene los datos del formulario guardados
     * 
     * @return array
     */
    public static function getFormData()
    {
        $aReturn = array();

        $aSession =& $_SESSION;

        if (isset($aSession[self::MSG_FORM_DATA_KEY])) {
            $aReturn = $aSession[self::MSG_FORM_DATA_KEY];
        }

        return $aReturn;
    }

    /**
     * Metodo que limpia los datos del formulario cuya validacion fallo
     * 
     * @return bool
     */
    public static function flushFormData()
    {
        if (isset($_SESSION[self::MSG_FORM_DATA_KEY])) {
            unset($_SESSION[self::MSG_FORM_DATA_KEY]);
        }

        return true;
    }

    /**
     * Metodo recursivo que se encarga de obtener un array asociativo con el valor indicado
     * 
     * @param array $aKeyParts Las partes, excepto la primera, de la key indicada
     * @param mixed $mValue    El valor a almacenar
     * 
     * @return array
     */
    private function _generateSessionArray($aKeyParts, $mValue)
    {
        if (0 === count($aKeyParts)) {
            return $mValue;
        }
        if (1 === count($aKeyParts)) {
            return array($aKeyParts[0] => $mValue);
        }

        return array(array_shift($aKeyParts) => $this->_generateSessionArray($aKeyParts, $mValue));
    }

    /**
     * Metodo que retorna las partes del key
     * 
     * @param string $sKey La key a partir
     * 
     * @return bool
     */
    private function _getKeyParts($sKey)
    {
        return explode('.', $sKey);
    }

}