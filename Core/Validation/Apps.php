<?php

/**
 * Apps.php
 * Archivo que contiene la clase Apps
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Validation
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Validation;

/**
 * Apps
 * Clase que se encarga de la comunicacion cliente <=> servicio
 *
 * @category Bootstrap
 * @package  AppManager
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  nolicense No license
 * @link     http://nolink.com
 */
class Apps
{
    /**
     * Properties
     */
    /**
     * Representa la lista de aplicaciones que se sabe que se accederan por la automatizacion de dispatcher y deben ser exluidas.
     */
    private static $aExcludedApps = array(
        // Esto es porque Chrome, al utilizar DevTools, intenta cargar un archivo .map dentro de 'Vendor'
        'Vendor', 
    );

    /**
     * Methods
     */
    
    /**
     * Verifica que la aplicacion indicada sea ejecutable
     * 
     * @param string $sAppName El nombre de la aplicacion a validar
     * 
     * @return bool
     */
    public static function isExecutable($sAppName)
    {
        if (!self::appExists($sAppName)) {
            throw new \Exception('La aplicacion "' . $sAppName . '" no existe');
        }

        $bIsExecutable = true;

        /**
         * Reviso que exista el archivo Main.php
         */
        if (!is_readable(PATH_AP_APPS . DS . $sAppName . DS . 'Main.php')) {
            $bIsExecutable = false;
        }

        return $bIsExecutable;
    }
    
    /**
     * Verifica que la aplicacion indicada este activa segun la informacion provista en su archivo .json
     * 
     * @param string $sAppName El nombre de la aplicacion a validar
     * 
     * @return bool
     */
    public static function isActive($sAppName)
    {
        if (!self::appExists($sAppName)) {
            throw new \Exception('La aplicacion "' . $sAppName . '" no existe');
        }

        $bIsActive = true;

        /**
         * Reviso que exista el archivo Main.php
         */
        $aAppInfo = \Core\Util\Json::decodeJsonFile(PATH_AP_APPS . DS . $sAppName . DS . 'AppInfo.json');
        if (!in_array('active', $aAppInfo) || !$aAppInfo['active']) {
            $bIsActive = false;
        }

        return $bIsActive;
    }

    /**
     * Metodo getter de la variable $aExcludedApps
     */
    public static function getExcludedApps()
    {
        return self::$aExcludedApps;
    }

    /**
     * Metodo que verifica que la aplicacion exista
     *
     * @param string $sAppName El nombre de la aplicacion a verificar
     *
     * @return bool
     */
    public static function appExists($sAppName)
    {
        return is_dir(PATH_AP_APPS . DS . $sAppName);
    }
}