<?php

/**
 * Type.php
 * Archivo que contiene la clase Type
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Validation
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Validation\Rules;

/**
 * Type
 * Clase que se encarga de validar que el tipo de dato ingresado no sea el indicado
 *
 * @category   AppManager
 * @package    Core
 * @subpackage Validation
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class Type extends \Core\Validation\Rules\Base
{
    /**
     * Properties
     */
    

    /**
     * Methods
     */
    public function validate()
    {
        if (!isset($this->sInputValue) || '' === $this->sInputValue) {
            throw new \Exception('El campo \'' . $this->sInputField . '\' debe contener un valor');
        }

        return true;
    }

}