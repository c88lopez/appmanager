<?php

/**
 * Mandatory.php
 * Archivo que contiene la clase Mandatory
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Validation
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Validation\Rules;

/**
 * Mandatory
 * Clase que se encarga de validar que el dato ingresado no sea vacio
 *
 * @category   AppManager
 * @package    Core
 * @subpackage Validation
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
class Mandatory extends \Core\Validation\Rules\Base
{
    /**
     * Properties
     */
    

    /**
     * Methods
     */
    public function validate()
    {
        if (!isset($this->sInputValue) || '' === $this->sInputValue) {
            throw new \Exception('El campo \'' . $this->sInputField . '\' debe contener un valor');
        }

        return true;
    }

}