<?php

/**
 * Base.php
 * Archivo que contiene la clase Base
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Validation
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Validation\Rules;

/**
 * Base
 * Clase que se encarga de validar que el dato ingresado no sea vacio
 *
 * @category   AppManager
 * @package    Core
 * @subpackage Validation
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
abstract class Base
{
    /**
     * Properties
     */
    /**
     * Contiene el nombre del campo a validar
     * @var string
     */
    protected $sInputField;
    /**
     * Contiene el valor a validar
     * @var string
     */
    protected $sInputValue;

    /**
     * Methods
     */
    /**
     * Metodo setter de la variable sInputValue
     * 
     * @param string $sValue El valor a guardar
     */
    public function setInputData($sFieldName, $sFieldValue)
    {
        $this->sInputField = $sFieldName;
        $this->sInputValue = $sFieldValue;
    }

}