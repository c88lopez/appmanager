<?php

/**
 * Input.php
 * Archivo que contiene la clase Input
 * 
 * PHP Version 5
 * 
 * @category   AppManager
 * @package    Core
 * @subpackage Validation
 * @author     Cristian Lopez <c88lopez@gmail.com>
 * @license    nolicense No license
 * @link       http://nolink.com
 */
namespace Core\Validation;

/**
 * Input
 * Clase que se encarga de validar los datos ingresados
 *
 * @category AppManager
 * @package  Validation
 * @author   Cristian Lopez <c88lopez@gmail.com>
 * @license  nolicense No license
 * @link     http://nolink.com
 */
class Input
{
    /**
     * Properties
     */
    /**
     * Representa una coleccion de reglas a aplicar
     * @var array
     */
    protected static $aRules = array();
    /**
     * Representa la lista de errores encontrados
     * @var array
     */
    protected static $aErrors = array();

    /**
     * Methods
     */

    /**
     * Metodo que se encarga de interpretar el array de validacion y retornar el resultado de las validaciones
     * 
     * @param  array $aFormValues      Representa la lista de campos del formulario enviado por POST a validar
     * @param  array $aValidationRules Representa la lista de reglas asociadas por campo
     * 
     * @return array
     */
    public static function validate($aFormValues, $aValidationRules)
    {
        if (empty($aFormValues)) {
            throw new \Exception('Se debe proveer un array no vacio de campos para la validacion');
        }

        if (empty($aValidationRules)) {
            throw new \Exception('Se debe proveer un array no vacio de validaciones a aplicar');
        }
var_dump('asd');
        self::setRules($aFormValues, $aValidationRules);
        $iRulesCount = count(self::$aRules);
        for ($i=0; $i < $iRulesCount; $i++) { 
            try {
                self::$aRules[$i]->validate();
            } catch (\Exception $e) {
                self::$aErrors[] = $e->getMessage();
            }
        }

        return self::$aErrors;
    }

    /**
     * Metodo que se encarga de obtener las instancias de las reglas provistas
     * 
     * @param array $aValidationRules Representa la lista de reglas asociadas por campo
     * 
     * @return bool
     */
    protected static function setRules($aFormValues, $aValidationRules)
    {
        $aValidationData = reset($aValidationRules);

        do {
            if (in_array('mandatory', $aValidationData)) {
                $oRuleInstance = new \Core\Validation\Rules\Mandatory();
                $oRuleInstance->setInputData(
                    key($aValidationRules), 
                    $aFormValues[key($aValidationRules)]
                );

                self::$aRules[] = $oRuleInstance;
            } else if (array_key_exists('type', $aValidationData)) {

            }

            unset($oRuleInstance);
        } while($aValidationData = next($aValidationRules));

        return true;
    }

}